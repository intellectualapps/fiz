//
//  SideMenuPresenter.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/12/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol SideMenuView: CommonView {
    func fillData()
}

protocol SideMenuViewPresenter {
    var sideMenuModel: SideMenuModel {get set}
    var view: SideMenuView {get set}
    //
    var avartarURL: URL? {get}
    var username: String {get}
    var percentCompleted: Double {get}
    var nameAttributedString: NSAttributedString {get}
    var totalDonation: Double {get}
    var goal: Float {get}
    var rank: Int {get}
    var points: Int {get}
    var rankDescriptionAttributedString: NSAttributedString {get}
    var notification: Int {get}
    var message: Int {get}
    //
    init(view: SideMenuView)
    func refresh()
}

extension SideMenuViewPresenter {
    var avartarURL: URL? {
        return sideMenuModel.user.avartarURL
    }
    var username: String {
        return sideMenuModel.user.username
    }
    var percentCompleted: Double {
        return sideMenuModel.donationProgress.percentCompleted
    }
    var nameAttributedString: NSAttributedString {
        return sideMenuModel.user.nameAttributedString
    }
    var totalDonation: Double {
        return sideMenuModel.donationProgress.totalDonation
    }
    var goal: Float {
        return sideMenuModel.donationProgress.goal
    }
    var rank: Int {
        return sideMenuModel.userBoard.rank
    }
    var points: Int {
        return sideMenuModel.userBoard.points
    }
    var rankDescriptionAttributedString: NSAttributedString {
        return sideMenuModel.userBoard.descriptionAttributedString
    }
    var notification: Int {
        return sideMenuModel.notification.count
    }
    var message: Int {
        return sideMenuModel.message.count
    }
}

class SideMenuPresenter: SideMenuViewPresenter {
    var sideMenuModel: SideMenuModel = SideMenuModel() {
        didSet {
            sideMenuModel.save()
            view.fillData()
        }
    }
    var view: SideMenuView
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    private let sevices: UserServices = UserServicesCenter()
    required init(view: SideMenuView) {
        self.view = view
        if let saved = SideMenuModel.load() {
            self.sideMenuModel = saved
        } else if let user = UserManager.shared.currentUser {
            self.sideMenuModel.user = user
        }
    }
    func refresh() {
        view.showLoading()
        sevices.getSideMenuStatus(username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let model):
                strongSelf.sideMenuModel = model
                let currentUser = UserManager.shared.currentUser
                currentUser?.portfolios = model.userCount.portfolioCount
                currentUser?.followers = model.userCount.followerCount
                currentUser?.donations = model.userCount.donationCount
                currentUser?.badges = model.notification.count
                UserManager.shared.currentUser = currentUser
            case .failure(_):
//                strongSelf.view.showAlert(error.localizedDescription)
                break
            }
        }
    }
}
