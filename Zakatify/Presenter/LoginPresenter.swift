//
//  LoginPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import FluidValidator

protocol LoginView: CommonView {
    func didLogin(user:UserInfo)
}

protocol LoginViewPresenter: class {
    init(view:LoginView)
    func loginWith(username: String, password:String, type:UserAPI.AuthenType)
    func loginbyEmail(username: String, password:String)
    func loginByFacebookWith(id:String)
    func loginByTwitterWith(id:String)
}

private class UserValidator: AbstractValidator<UserInfo> {
    override init() {
        super.init()
        self.addValidation(withName: "password") { (user) -> (Any?) in
            user.password
            }.addRule(Length(min: 6, max: 15))
        self.addValidation(withName: "username") { (user) -> (Any?) in
            user.username
            }.addRule(Length(min: 3, max: 20))
    }
}

class LoginPresnter: LoginViewPresenter {
    unowned let view:LoginView
    let service:UserServices = UserServicesCenter()
    required init(view: LoginView) {
        self.view = view
    }
    
    func loginWith(username: String, password: String, type: UserAPI.AuthenType) {
        switch type {
        case .email:
            loginbyEmail(username: username, password: password)
        case .facebook:
            loginByFacebookWith(id: username)
        case .twitter:
            loginByTwitterWith(id: username)
        }
    }
    func loginbyEmail(username: String, password:String) {
        let user = UserInfo()
        user.username = username
        user.password = password
        let validator = UserValidator()
        let _ = validator.validate(object: user)
        let failMessage = validator.allErrors
        if let key = validator.allErrors.failingFields().first {
            if let message = failMessage.failMessageForPath(key)?.errors.first?.compact {
                self.view.showAlert(message.replacingOccurrences(of: "This", with: "\(key)"))
            }
            return
        }
        view.showLoading()
        service.login(username, password: password) {[weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                UserManager.shared.currentUser = user
                UserManager.shared.authenToken = user.authToken
                strongSelf.view.didLogin(user: user)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    func loginByFacebookWith(id:String) {
        view.showLoading()
        service.loginFaceBook(id) {[weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                strongSelf.view.didLogin(user: user)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    func loginByTwitterWith(id:String) {
        view.showLoading()
        service.loginTwitter(id) {[weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                strongSelf.view.didLogin(user: user)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
}
