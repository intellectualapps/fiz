//
//  ForgetPasswordPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
protocol ForgetPasswordView: CommonView {
    func didRequestResetPassword()
}

protocol ForgetPasswordViewPresenter {
    init(view:ForgetPasswordView)
    func requestResetPassword(email: String)
}

class ForgetPasswordPresenter: ForgetPasswordViewPresenter {
    unowned let view:ForgetPasswordView
    let service:UserServices = UserServicesCenter()
    required init(view: ForgetPasswordView) {
        self.view = view
    }

    func requestResetPassword(email: String) {
        view.showLoading()
        service.forgotPassword(email) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(_):
                strongSelf.view.didRequestResetPassword()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
}
