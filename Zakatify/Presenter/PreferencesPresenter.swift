//
//  PreferencesPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol PreferenceView: CommonView {
    func fillData()
    func saveSuccess()
}

protocol PreferenceViewPresenter {
    var view:PreferenceView {set get}
    var nisab: Int {get}
    var amount: Float {get}
    var frequency: Zakat.frequency {get}
    var w:Int {get set}
    var d:Int {get set}
    var nw:Int {get set}
    var z:NSNumber {get set}
    init(view:PreferenceView)
    func calculateW(ce:Int, gs:Int, re:Int, i:Int)
    func calculateD(pu:Int, l:Int)
    func getZakatGoal()
    func addZakatGoal(amount:Float)
    func updateZakatGoal(amount:Float)
    func deleteZakatGoal()
}

class PreferencePresenter: PreferenceViewPresenter {
    unowned var view: PreferenceView
    var nisab: Int = 1000
    var amount: Float {
        set {
            zakat.amount = newValue
            view.fillData()
        }
        get {
            return zakat.amount

        }
    }
    
    var frequency: Zakat.frequency {
        set {
            zakat.frequencyId = newValue.rawValue
            view.fillData()
        }
        get {
            return Zakat.frequency(rawValue: zakat.frequencyId) ?? Zakat.frequency.auto
        }
    }
    
    var w:Int = 0
    var d:Int = 0
    var nw:Int = 0
    var z:NSNumber {
        get {
            return NSNumber(value: self.amount)
        }
        set {
            self.amount = newValue.floatValue
        }
    }
    
    var zakat: Zakat = Zakat() {
        didSet {
            zakat.save()
            view.fillData()
        }
    }
    
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    
    let services: ZakatServices = ZakatServicesCenter()
    required init(view: PreferenceView) {
        self.view = view
        self.getZakatGoal()
    }
    
    func calculateW(ce:Int, gs:Int, re:Int, i:Int) {
        
    }
    func calculateD(pu:Int, l:Int) {
        
    }
    
    func getZakatGoal() {
        view.showLoading()
        services.get(username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let zakat):
                strongSelf.zakat = zakat
            case .failure(let error):
                strongSelf.zakat = Zakat()
                if error.code == 400 {
                    return
                } else {
                    strongSelf.view.showAlert(error.localizedDescription)
                }
            }
        }
    }
    
    func addZakatGoal(amount:Float) {
        self.amount = amount
        if zakat.username.isEmpty {
            view.showLoading()
            services.add(zakat: zakat, username: self.username, complete: { [weak self] (result) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.view.hideLoading()
                switch result {
                case .success(let zakat):
                    strongSelf.zakat = zakat
                    strongSelf.view.saveSuccess()
                case .failure(let error):
                    strongSelf.view.showAlert(error.localizedDescription)
                }
            })
        } else {
            updateZakatGoal(amount: amount)
        }
    }
    
    func updateZakatGoal(amount:Float) {
        view.showLoading()
        services.update(zakat: zakat, username: self.username, complete: { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let zakat):
                strongSelf.zakat = zakat
                strongSelf.view.saveSuccess()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        })
    }
    
    func deleteZakatGoal() {
        view.showLoading()
        services.delete(username: username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success( _):
                strongSelf.view.showAlert("success")
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
}
