//
//  ZakatifierPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

protocol ZakatifierDetailsView: CommonView {
    func dataChanged()
}

protocol ZakatifierDetailsViewPresenter {
    var model: UserPublicProfile {get set}
    var reviews: [CharityReview] {get set}
    var view:ZakatifierDetailsView {get set}
    //
    var avartarURL: URL? {get}
    var username: String {get}
    var percentCompleted: Double {get}
    var detailPercentCompleted: Double {get}
    var nameAttributedString: NSAttributedString {get}
    var totalDonation: Double {get}
    var goal: Float {get}
    var rank: Int {get}
    var points: Int {get}
    var rankDescriptionAttributedString: NSAttributedString {get}
    var notification: Int {get}
    var message: Int {get}
    
    init(view:ZakatifierDetailsView, model: UserPublicProfile)
    func sendMessage(message:String)
    func follow()
    func unfollow()
}

extension ZakatifierDetailsViewPresenter {
    var avartarURL: URL? {
        return model.user.avartarURL
    }
    var username: String {
        return model.user.username
    }
    var percentCompleted: Double {
        return model.donationProgress.percentCompleted
    }
    var detailPercentCompleted: Double {
        return model.donationProgess.percentCompleted
    }
    var nameAttributedString: NSAttributedString {
        return model.user.nameAttributedString
    }
    var totalDonation: Double {
        return model.donationProgress.totalDonation
    }
    var goal: Float {
        return model.donationProgress.goal
    }
    var rank: Int {
        return model.userBoard.rank
    }
    var points: Int {
        return model.userBoard.points
    }
    var rankDescriptionAttributedString: NSAttributedString {
        return model.userBoard.descriptionAttributedString
    }
    var notification: Int {
        return model.notification.count
    }
    var message: Int {
        return model.message.count
    }
}

class ZakatifierDetailsPresenter: ZakatifierDetailsViewPresenter {
    var model: UserPublicProfile
    var reviews: [CharityReview] = []
    var reviewPageCount: Page = PageCount()

    var service: ZakatifierServicesCenter = ZakatifierServicesCenter()
    unowned var view: ZakatifierDetailsView
    required init(view: ZakatifierDetailsView, model: UserPublicProfile) {
        self.view = view
        self.model = UserPublicProfile()
        self.model = model
    }

    func sendMessage(message:String) {
        view.showLoading()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.view.hideLoading()
        })
    }
    
    func follow() {
        view.showLoading()
        let username = UserManager.shared.currentUser?.username ?? ""
        service.follow(username: username, followed: model.user.username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(_):
                strongSelf.model.isFollowing.state = true
                strongSelf.view.dataChanged()
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }

    func unfollow() {
        view.showLoading()
        let username = UserManager.shared.currentUser?.username ?? ""
        service.unfollow(username: username, followed: model.user.username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(_):
                strongSelf.model.isFollowing.state = false
                strongSelf.view.dataChanged()
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }

    func refresh() {
        view.showLoading()
        service.userPublicProfile(username: model.user.username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            strongSelf.getReviews()
            switch result {
            case .success(let model):
                strongSelf.model = model
                strongSelf.view.dataChanged()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    var requestReviews: Cancellable?
    func getReviews() {
        view.showLoading()
        requestReviews = service.charityReviews(username: model.user.username, page: reviewPageCount.page, size: reviewPageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            strongSelf.requestReviews = nil
            switch result {
            case .success((reviews:let reviews, page:let page ,size:let size, total:let total)):
                strongSelf.reviews = reviews
                strongSelf.reviewPageCount = PageCount(page: page, size: size, total: total)
                strongSelf.view.dataChanged()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func loadmoreReviews() {
        if shouldLoadmoreReviews() == false || requestReviews != nil {
            return
        }
        view.showLoading()
        requestReviews = service.charityReviews(username: model.user.username, page: reviewPageCount.page + 1, size: reviewPageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            strongSelf.requestReviews = nil
            switch result {
            case .success((reviews:let reviews, page:let page ,size:let size, total:let total)):
                strongSelf.reviews += reviews
                strongSelf.reviewPageCount = PageCount(page: page, size: size, total: total)
                strongSelf.view.dataChanged()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    func shouldLoadmoreReviews() -> Bool {
        return reviews.count < reviewPageCount.total && reviews.count > 0
    }
    
}
