//
//  ReviewViewController.swift
//  Zakatify
//
//  Created by MinhTran on 11/14/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import Cosmos
import KMPlaceholderTextView

class ReviewViewController: UIViewController {
    @IBOutlet weak var rateControl: CosmosView!
    @IBOutlet weak var tv_comment: KMPlaceholderTextView!
    
    var presenter: CharityDetailPresenter?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submit(_ sender: Any) {
        let rate = Int(rateControl.rating)
        let comment = tv_comment.text ?? ""
        presenter?.review(rate: rate, comment: comment)
    }
}

extension ReviewViewController: CharityDetailView {
    func userDidAddToPorfolio() {
        
    }
    
    func userDidDonate() {
        
    }
    
    func dataChanged() {
        
    }
    
    var refTableView: UITableView? {
        return nil
    }
    
    var refPresenter: TableViewPresenter? {
        return nil
    }
    
    func userDidWriteReview() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ReviewViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let touchedView = touch.view {
            for subview in view.subviews {
                if touchedView.isDescendant(of: subview) {
                    return false
                }
            }
        }
        return true
    }
}
