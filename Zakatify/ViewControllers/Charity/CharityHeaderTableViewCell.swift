//
//  CharityHeaderTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/7/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
protocol CharityHeaderTableViewCellDelegate: class {
    func clickDonate()
    func clickAddPortfolio()
    func clickWebsite()
    func clickEmail()
}

class CharityHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var uv_avatar_boder: UIView!
    @IBOutlet weak var iv_avatar: UIImageView!
    
    @IBOutlet weak var lb_charityName: UILabel!
    @IBOutlet weak var lb_rate: UILabel!
    
    @IBOutlet weak var uv_totalMoney_boder: UIView!
    @IBOutlet weak var tf_totoalMoney: NumberTextFeild!
    
    @IBOutlet weak var bt_add: Button!
    
    @IBOutlet weak var uv_donor: UIView!
    @IBOutlet weak var uv_donor_avatars: UIView!
    @IBOutlet weak var lb_donor_status: AutocConstraintLabel!
    @IBOutlet weak var collectionView_donor_avatar: UICollectionView!
    @IBOutlet weak var collectionViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lb_charityDescription: UILabel!
    @IBOutlet weak var lbDonorStatusWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addressHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lb_ein: UILabel!
    @IBOutlet weak var bt_report: UIButton!
    
    @IBOutlet weak var lb_address: UILabel!
    @IBOutlet weak var lb_phoneNumber: UILabel!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var emailWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var websiteWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagView: TagListView! {
        didSet {
            tagView.alignment = .center
            tagView.delegate = self
        }
    }
    
    var presenter: CharityDetailViewPresenter? {
        didSet {
            dataChanged()
        }
    }
    
    var bubblePictures: BubblePictures!
    
    weak var delegate: CharityHeaderTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uv_avatar_boder.addDashedBorder(color: UIColor.blueBorderColor, width: 3, space: 3)
        uv_totalMoney_boder?.layer.cornerRadius = 5.0
        uv_totalMoney_boder?.layer.masksToBounds = true
        let iv = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 38))
        iv.image = #imageLiteral(resourceName: "ic-dolar")
        tf_totoalMoney?.leftView = iv
        tf_totoalMoney?.leftViewMode = .always
        tf_totoalMoney?.isUserInteractionEnabled = false
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func clickDonate(_ sender: Any) {
        delegate?.clickDonate()
    }

    @IBAction func clickAdd(_ sender: Any) {
        delegate?.clickAddPortfolio()
    }
    @IBAction func clickReport(_ sender: Any) {
        guard let presenter = presenter, let url = URL(string:presenter.charity.reportUrl) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func clickWebsite(_ sender: UIButton) {
        delegate?.clickWebsite()
    }
    
    @IBAction func clickEmail(_ sender: UIButton) {
        delegate?.clickEmail()
    }
    
}

extension CharityHeaderTableViewCell {
   
    func dataChanged() {
        guard let presenter = presenter else {
            return
        }
        
        if presenter.charity.totalMoney == 0 {
            uv_totalMoney_boder?.isHidden = true
        }
        categoryFillData()
        lb_charityName.text = presenter.charityName
        tf_totoalMoney?.text = "\(presenter.charity.totalMoney)"
        lb_charityDescription.text = presenter.charityDesription
        if let url = URL(string: presenter.charityLogoUrl) {
            iv_avatar.sd_setImage(with: url)
        }
        var rateText = ""
        for _ in 0..<presenter.charityRate {
            rateText += "★"
        }
        for _ in rateText.count..<5 {
            rateText += "☆"
        }
        lb_rate.text = rateText
        
        if presenter.userPortfolio == true {
            bt_add.setTitle("Remove from Portfolio", for: UIControlState.normal)
        } else {
            bt_add.setTitle("Add to Portfolio", for: UIControlState.normal)
        }
        configAvatarCollectionView(presenter: presenter)
//        collectionViewWidthConstraint.constant = (uv_donor.bounds.height - 7) * CGFloat(collectionView_donor_avatar.numberOfItems(inSection: 0))
        lb_donor_status.text = presenter.donateDescription
//        lbDonorStatusWidthConstraint.constant = lb_donor_status.totalWidth()
        lb_ein.text = "EIN " + presenter.charity.ein
        lb_address.text = presenter.charity.address
        lb_phoneNumber.text = presenter.charity.phoneNumber
        if presenter.charityWebsite == nil {
            websiteButton?.isHidden = true
            websiteWidthConstraint?.constant = 0
        }
        if presenter.charityEmail.isEmpty {
            emailButton?.isHidden = true
            emailWidthConstraint?.constant = 0
        }

        let address = presenter.charity.address
        let font = lb_address.font ?? UIFont.systemFont(ofSize: 17)
        let boundSize = CGSize(width: UIScreen.main.bounds.width - 60,
                               height: CGFloat.greatestFiniteMagnitude)
        let size = address.sizeWithBoundSize(boundSize: boundSize, option: NSStringDrawingOptions.usesLineFragmentOrigin, lineBreakMode: NSLineBreakMode.byWordWrapping, font: font)
        addressHeightConstraint?.constant = size.height + 20
    }
    
    private func configAvatarCollectionView(presenter: CharityDetailViewPresenter) {
        let configFiles = getConfigFiles(presenter: presenter)
        let layoutConfigurator = BPLayoutConfigurator(backgroundColorForTruncatedBubble: UIColor.gray,
                                                      fontForBubbleTitles: UIFont(name: "HelveticaNeue-Light", size: 16.0)!,
                                                      colorForBubbleBorders: .white,
                                                      colorForBubbleTitles: .white,
                                                      maxCharactersForBubbleTitles: 5,
                                                      maxNumberOfBubbles: 5,
                                                      direction: .rightToLeft,
                                                      alignment: .center)
        
        bubblePictures = BubblePictures(collectionView: collectionView_donor_avatar,
                                        configFiles: configFiles,
                                        layoutConfigurator: layoutConfigurator)
    }
    
    private func getConfigFiles(presenter: CharityDetailViewPresenter) -> [BPCellConfigFile] {
        var fileArr = [BPCellConfigFile]()
        for url in presenter.donorsAvatarUrls {
            let cell = BPCellConfigFile(imageType: BPImageType.URL(url), title: "")
            fileArr.append(cell)
        }
        if fileArr.isEmpty {
            let cell = BPCellConfigFile(imageType: BPImageType.URL(UserManager.shared.currentUser?.profileUrl ?? ""),
                                        title: "")
            fileArr.append(cell)
        }
        return fileArr
    }
}

// MARK: - CategoryView
extension CharityHeaderTableViewCell: CategoryView {
    func added() {
        
    }
    
    func categoryFillData() {
        guard let presenter = presenter,
            let tagView = tagView else {
                return
        }
        tagView.removeAllTags()
        let tags = presenter.charity.tags.map { (tag) in
            return tag.description
        }
        tagView.addTags(tags)
    }
}

extension CharityHeaderTableViewCell: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        
    }
}
