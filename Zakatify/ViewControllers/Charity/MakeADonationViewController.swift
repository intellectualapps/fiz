//
//  MakeADonationViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class MakeADonationViewController: UIViewController, CharityDetailView, CreditCardView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var uv_avatar_border: UIView! {
        didSet {
            uv_avatar_border?.addDashedBorder(color: UIColor.blueBorderColor, width: 3, space: 3)
        }
    }
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_rate: UILabel!

    @IBOutlet weak var tf_money: NumberTextFeild! {
        didSet {
            let iv = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 38))
            iv.image = #imageLiteral(resourceName: "ic-dolar")
            tf_money.leftView = iv
            tf_money.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView?.delegate = self
            tableView?.dataSource = self
        }
    }
    
    var refTableView: UITableView? {
        return tableView
    }
    
    var crediCardPresenter: CreditCardViewPresenter?
    var refPresenter: TableViewPresenter? {
        return crediCardPresenter
    }
    var selectedPaymet: Payment?
    
    var charityDetail: CharityDetail?
    var presenter: CharityDetailViewPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addLeftCloseButton()
        
        self.tableView.estimatedRowHeight = 70
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(UINib(nibName: "PaymentTableViewCell", bundle: nil) , forCellReuseIdentifier: PaymentTableViewCell.identifier)
        
        crediCardPresenter = CreditCardPresenter(view: self)
        if let charityDetail = self.charityDetail {
            presenter = CharityDetailPresenter(view: self, model: charityDetail)
            self.fillData()
        }
        
        setupUI()
    }
    
    func setupUI() {
        tf_money.delegate = self
    }
    
    func saveSuccess() {
        
    }
    
    func userDidAddToPorfolio() {
        
    }
    func userDidDonate() {
        
    }
    func userDidWriteReview() {
        
    }
    
    func dataChanged() {
        
    }
    
    func fillData() {
        guard let presenter = presenter else {
            return
        }
        lb_name.text = presenter.charityName
        if let url = URL(string: presenter.charityLogoUrl) {
            iv_avatar.sd_setImage(with: url)
        }
        var rateText = ""
        for _ in 0..<presenter.charityRate {
            rateText += "⭑"
        }
        lb_rate.text = rateText
    }
    
    func didDonate() {
        
    }
    
    func refreshing() {
        showLoading()
    }
    
    func refreshed() {
        hideLoading()
        guard let _ = crediCardPresenter else {
            return
        }
        tableView.reloadData()
    }
    
    @IBAction func clickDonate(_ sender: Any) {
        guard let number = tf_money.number, let payment = selectedPaymet else {
            return
        }
        presenter?.donate(money: number.intValue , payment: payment, completion: {[weak self] in
            self?.dismiss(animated: true, completion: nil)
        })
    }
}

extension MakeADonationViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let presenter = crediCardPresenter else {
            return 0
        }
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = crediCardPresenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentTableViewCell.identifier, for: indexPath) as? PaymentTableViewCell else {
            return UITableViewCell()
        }
       
        if let payment = crediCardPresenter?.paymentAt(indexPath: indexPath) {
            cell.payment = payment
            if payment.isPrimary == 1 {
                selectedPaymet = payment
            }
        }
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let payment = crediCardPresenter?.paymentAt(indexPath: indexPath) {
            selectedPaymet = payment
        }
        tableView.reloadData()
    }
}

// MARK: - PaymentTableViewCellDelegate
extension MakeADonationViewController: PaymentTableViewCellDelegate {
    func clickMakePrimary(payment: Payment?, cell: PaymentTableViewCell) {
        
    }
    
    func clickDelete(payment: Payment?, cell: PaymentTableViewCell) {
        
    }
}

// MARK: - UITextFieldDelegate
extension MakeADonationViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let number = Int(textField.text!.replacingOccurrences(of: ",", with: "")), number >= 1000 {
            let alert = UIAlertController(title: nil, message: "You are planning to make a one-time donation of $\(textField.text!) or more to this charity. Are you sure you want to do this", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .destructive, handler: { _ in
                textField.text = "0"
            })
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: nil)
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    }
}
