//
//  EditPortfolioViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/19/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift
import SVPullToRefresh

class EditPortfolioViewController: HomeViewController {
    
    override func viewDidLoad() {
        self.addBackButtonDefault()
        self.addSwipeRight()
        tableView.register(UINib(nibName: "CharityDetailTableViewCell", bundle: nil), forCellReuseIdentifier: CharityDetailTableViewCell.identifier)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.contentInset.top = 0
        presenter = UserPortfolioPresenter(view: self)
        presenter?.charities
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (data) in
                if let strongself = self , data.count > 0 {
                    strongself.tableView.reloadData()
                    strongself.tableView.infiniteScrollingView.stopAnimating()
                }
            }).disposed(by: disposed)
        
        tableView.addInfiniteScrolling(actionHandler: {[weak self] in
            if let strongself = self {
                strongself.presenter?.loadmore()
            }
        })
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = nil
        } else {
            self.tableView.tableHeaderView = UIView()
        }
        addTableViewHeader()
    }
    
    func addTableViewHeader() {
        let lb_header = UITextView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 20, height: 60))
        lb_header.isEditable = false
        lb_header.isSelectable = false
//        let tv_header = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 20, height: 60))
        lb_header.font = UIFont.systemFont(ofSize: 15)
        lb_header.textColor = UIColor.lightGray
        lb_header.textAlignment = .center
        lb_header.text = "If you have automatic donations selected in the zakat goal settings, around the first of each month, we will distribute a portion of your annual zakat goal equally to the following charities so that you hit your goal by the end of the calendar year. Uncheck to remove a charity from your portfolio"
        lb_header.sizeToFit()
        tableView.tableHeaderView = lb_header
    }

}
