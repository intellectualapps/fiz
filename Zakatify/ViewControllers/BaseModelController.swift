//
//  BaseModelController.swift
//  Zakatify
//
//  Created by Thang Truong on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//
import UIKit

class BaseModelController: NSObject, UIPageViewControllerDataSource {
    
    var viewControllers = [UIViewController]()

    func setupViewControllers() {
        let storyboard = UIStoryboard(name: "RegisterLoginEmail", bundle: nil)
        let register = storyboard.instantiateViewController(withIdentifier: EmailRegisterVC.identifier) as! EmailRegisterVC

        let login = storyboard.instantiateViewController(withIdentifier: EmailLoginViewController.identifier) as! EmailLoginViewController
        viewControllers = [register, login]
    }

    func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        // Return the data view controller for the given index.
        if (self.viewControllers.count == 0) || (index >= self.viewControllers.count) || (index < 0) {
            return nil
        }
        return viewControllers[index]
    }
    
    func indexOfViewController(_ viewController: UIViewController) -> Int? {
        return viewControllers.index(of: viewController)
    }
    
    // MARK: - Page View Controller Data Source
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard var index = self.indexOfViewController(viewController) else { return nil}
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard var index = self.indexOfViewController(viewController) else { return nil}
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        if index == self.viewControllers.count {
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
}
