//
//  EmailLoginViewController.swift
//  Zakatify
//
//  Created by Thang Truong on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class EmailLoginViewController: UITableViewController {
    static var identifier = "EmailLoginViewController"

    @IBOutlet weak var tf_username: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    var presenter: LoginPresnter?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = LoginPresnter(view: self)
        registerNotification()
    }
    
    func registerNotification() {
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

    func doLogin() {
        guard let username = tf_username.text, let password = tf_password.text else {
            return
        }
        presenter?.loginbyEmail(username: username, password: password)
    }
    
    @IBAction func onClickForgotEmail() {
        NotificationCenter.default.post(name: NotificationKey.showForgotPasswordVC, object: nil)
    }
    
    // TODO : assign a.Thang
    fileprivate func gotoHomeViewController() {
        AppDelegate.shareInstance().gotoHomeViewController()
    }
}
extension EmailLoginViewController: LoginView {
    func didLogin(user:UserInfo) {
        gotoHomeViewController()
        NotificationCenter.default.removeObserver(self)
        self.parent?.removeFromParentRecursive()
        self.presenter = nil
    }
}
