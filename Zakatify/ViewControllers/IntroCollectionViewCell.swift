//
//  IntroCollectionViewCell.swift
//  Layout
//
//  Created by Trương Thắng on 5/27/17.
//  Copyright © 2017 Trương Thắng. All rights reserved.
//

import UIKit

class IntroCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageIntro: UIImageView!
    @IBOutlet weak var descriptionLable: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var shadow: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.borderColor = UIColor.gray.cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 4
        shadow.backgroundColor = UIColor.lightGray.withAlphaComponent(0.75)
        shadow.layer.cornerRadius = 4
    }
    
    override func prepareForReuse() {
        imageIntro.image = nil
        titleLabel.text = ""
        descriptionLable.text = ""
    }
    
}
