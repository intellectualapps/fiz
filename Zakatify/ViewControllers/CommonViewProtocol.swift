//
//  File.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import UserNotifications
import MBProgressHUD

public protocol CommonView: NSObjectProtocol {
    func showLoading()
    func hideLoading()
    func showAlert(_ message:String)
}

@objc extension UIViewController: CommonView {
    public func showLoading() {
        DispatchQueue.main.async {
            if let root = AppDelegate.shareInstance().window?.rootViewController?.view {
                let subviews = root.subviews
                var existed = false
                subviews.forEach { (view) in
                    if view.isKind(of: MBProgressHUD.self) {
                        existed = true
                    }
                }
                if !existed {
                    AppDelegate.shareInstance().window?.isUserInteractionEnabled = false
                    MBProgressHUD.showAdded(to: root, animated: true)
                }
            }
        }
    }
    
    public func hideLoading() {
        DispatchQueue.main.async {
            if let root = AppDelegate.shareInstance().window?.rootViewController?.view {
                AppDelegate.shareInstance().window?.isUserInteractionEnabled = true
                MBProgressHUD.hide(for: root, animated: true)
                let subviews = root.subviews
                subviews.forEach { (view) in
                    if view.isKind(of: MBProgressHUD.self) {
                        view.removeFromSuperview()
                    }
                }
            }
        }
    }
    public func showAlert(_ message:String) {
//        let title = "Message from Zakatify"
//        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        let ok = UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil)
//        alert.addAction(ok)
//        self.present(alert, animated: true, completion: nil)
        
        let content = UNMutableNotificationContent()
        content.body = message
        
        //Set the trigger of the notification -- here a timer.
        let trigger = UNTimeIntervalNotificationTrigger(
            timeInterval: 0.01,
            repeats: false)
        
        //Set the request for the notification from the above
        let request = UNNotificationRequest(
            identifier: "Local.Alert",
            content: content,
            trigger: trigger
        )
        
        //Add the notification to the currnet notification center
        UNUserNotificationCenter.current().add(
            request, withCompletionHandler: nil)
    }
}

//extension UITableViewController {
//    
//    public override func showLoading() {
//        guard let superView = self.view.superview else {
//            MBProgressHUD.showAdded(to: self.view, animated: true)
//            return
//        }
//        MBProgressHUD.showAdded(to: superView, animated: true)
//    }
//    
//    public override func hideLoading() {
//        guard let superView = self.view.superview else {
//            MBProgressHUD.hide(for: self.view, animated: true)
//            return
//        }
//        MBProgressHUD.hide(for: superView, animated: true)
//    }
//
//}

@objc extension UIView: CommonView {
    public func showLoading() {
        MBProgressHUD.showAdded(to: self, animated: true)
    }
    
    public func hideLoading() {
        MBProgressHUD.hide(for: self, animated: true)
    }
    public func showAlert(_ message:String) {
        //        let title = "Message from Zakatify"
        //        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        //        let ok = UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil)
        //        alert.addAction(ok)
        //        self.present(alert, animated: true, completion: nil)
        
        let content = UNMutableNotificationContent()
        content.body = message
        
        //Set the trigger of the notification -- here a timer.
        let trigger = UNTimeIntervalNotificationTrigger(
            timeInterval: 0.01,
            repeats: false)
        
        //Set the request for the notification from the above
        let request = UNNotificationRequest(
            identifier: "Local.Alert",
            content: content,
            trigger: trigger
        )
        
        //Add the notification to the currnet notification center
        UNUserNotificationCenter.current().add(
            request, withCompletionHandler: nil)
    }
}
