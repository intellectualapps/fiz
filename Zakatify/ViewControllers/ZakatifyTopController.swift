//
//  ZakatifyTopController.swift
//  Zakatify
//
//  Created by center12 on 5/31/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ZakatifyTopController: UIViewController {
    //MARK: Data Models
    //-----------------------
    
    //MARK: Local Variables
    //-----------------------
    var showLifeView: Bool = false
    var disposed = DisposeBag()
    //MARK: UI Elements
    //-----------------------
    @IBOutlet weak var lifeButton: UIButton!
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var lineView: UIView!

    @IBOutlet weak var lifeContainer: UIView!
    @IBOutlet weak var monthContainer: UIView!
    
    //MARK: UIViewController
    //-----------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showLifeOrMonthView(isLife: showLifeView, animated: false, isForce: true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.addBackButtonDefault()
        self.addSwipeRight()
    }
    
    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }
    
    func lifeViewController() -> LifeTimeTabController? {
        for child in self.children {
            if let controller = child as? LifeTimeTabController {
                return controller
            }
        }
        return nil
    }
    
    func monthViewController() -> ThisMonthTabController? {
        for child in self.children {
            if let controller = child as? ThisMonthTabController {
                return controller
            }
        }
        return nil
    }
    //MARK: View Setup
    //-----------------------
    func setupView() {
        self.title = "Top Zakatifiers"
        
    }
    //MARK: Events
    //-----------------------
    @IBAction func lifeBtnAction(_ sender: Any) {
        self.showLifeOrMonthView(isLife: true, animated: true)
        self.view?.bringSubviewToFront(self.lifeContainer)
        showLifeView = true
    }
    
    @IBAction func monthBtnAction(_ sender: Any) {
        self.showLifeOrMonthView(isLife: false, animated: true)
        self.view?.bringSubviewToFront(self.monthContainer)
        showLifeView = false
    }
    
    func showLifeOrMonthView(isLife: Bool, animated: Bool, isForce: Bool = false) {
        lifeContainer?.isHidden = !isLife
        monthContainer?.isHidden = isLife
        self.changeTitleColorAfterChangeTab(left: lifeButton, right: monthButton, leftActive: !isLife)
        let centerX = !isLife ? lifeButton.center.x : monthButton.center.x
        UIView.animate(withDuration: 0.2) {[weak self] in
            self?.lineView?.center.x = centerX
            
        }
        if(isLife) {
            self.lifeViewController()?.getUsersFromServer()
        }else {
            self.monthViewController()?.getUsersFromServer()
        }
    }
    
    func changeTitleColorAfterChangeTab(left: UIButton?, right: UIButton?, leftActive: Bool = false) {
        let inactiveColor = UIColor.init(hex: "9B9B9B")
        let activeColor = UIColor.init(hex: "000000")
        left?.setTitleColor(leftActive ? activeColor : inactiveColor, for: .normal)
        right?.setTitleColor(!leftActive ? activeColor : inactiveColor, for: .normal)
    }
}
