//
//  CharitySearchService.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/9/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class CharitySearchService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results: [String: Any] = [:]
        var zakatifiers: [ZakatifierInfo] = []
        if let root = response as? [String: Any] {
            results["charities"] = CharitiesInfo(dict: root)
            if let zakatifiersDict = root["zakatifiers"] as? [[String: Any]] {
                for zakatifierDict in zakatifiersDict {
                    if let zakatifier = ZakatifierInfo(JSON: zakatifierDict) {
                        zakatifiers.append(zakatifier)
                    }
                }
                results["zakatifiers"] = zakatifiers
            }
        }
        super.onFinish(results, error: error, completion: completion)
    }
}
