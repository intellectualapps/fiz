//
//  ParseResponse.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

let Domain = "Moya"
extension Moya.Response {
    enum ResponseKey {
        enum Error:String {
            case status
            case code
            case message
        }
    }
    
    var defaultAPIError: NSError {
        return NSError(domain: Domain, code: 0, userInfo: [NSLocalizedDescriptionKey: "Somthing went wrong with API service"])
    }
    
    /**
     Returns the `Response` if the `statusCode` falls within the specified range.
     
     - parameters:
     - statusCodes: The range of acceptable status codes.
     - throws: `MoyaError.statusCode` when others are encountered.
     */
    public func filter(statusCodes: ClosedRange<Int>) throws -> Response {
        guard statusCodes.contains(statusCode) else {
            throw MoyaError.statusCode(self)
        }
        
        do {
            if let json = try self.mapJSON(failsOnEmptyData: false) as? [String:Any] {
                if let code = json[ResponseKey.Error.code.rawValue] as? Int {
                    if let message = json[ResponseKey.Error.message.rawValue] as? String {
                        throw NSError(domain: Domain, code: code, userInfo: [NSLocalizedDescriptionKey:message])
                    } else {
                        throw defaultAPIError
                    }
                }
            }
        } catch {
            if let moyaError = error as? MoyaError {
                switch moyaError {
                case .underlying(let error, _):
                    throw error
                default:
                    throw NSError(domain: Domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription])
                }
            } else {
                throw error
            }
        }
        return self
    }
    
    /**
     Returns the `Response` if it has the specified `statusCode`.
     
     - parameters:
     - statusCode: The acceptable status code.
     - throws: `MoyaError.statusCode` when others are encountered.
     */
    public func filter(statusCode: Int) throws -> Response {
        return try filter(statusCodes: statusCode...statusCode)
    }
    
    /**
     Returns the `Response` if the `statusCode` falls within the range 200 - 299.
     
     - throws: `MoyaError.statusCode` when others are encountered.
     */
    public func filterSuccessfulStatusCodes() throws -> Response {
        return try filter(statusCodes: 200...299)
    }
    
    func mapValue<T>(path:String? = nil) throws -> T {
        do {
            let filtedResponce = try self.filterSuccessfulStatusCodes()
            let json = try filtedResponce.mapJSON(failsOnEmptyData: false)
            var jsonToMap = json
            if let path = path {
                if let objectJson = (json as AnyObject).value(forKeyPath: path) {
                    jsonToMap = objectJson
                }
            }
            guard let map = jsonToMap as? T else {
                throw NSError(domain: Domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey:HTTPURLResponse.localizedString(forStatusCode: statusCode)])
            }
            return map
        }
        catch {
            if let moyaError = error as? MoyaError {
                switch moyaError {
                case .underlying(let error, _):
                    throw error
                default:
                    throw NSError(domain: Domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription])
                }
            } else {
                throw error
            }
        }
    }

    func mapObject<T: Mappable>(path:String? = nil) throws -> T {
        do {
            let filtedResponce = try self.filterSuccessfulStatusCodes()
            let json = try filtedResponce.mapJSON(failsOnEmptyData: false)
            var jsonToMap = json
            if let path = path {
                if let objectJson = (json as AnyObject).value(forKeyPath: path) {
                    jsonToMap = objectJson
                }
            }
            guard let map = jsonToMap as? [String:Any] else {
                throw NSError(domain: Domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey:HTTPURLResponse.localizedString(forStatusCode: statusCode)])
            }
            if let object = Mapper<T>().map(JSON: map) {
                return object
            }
        }
        catch {
            if let moyaError = error as? MoyaError {
                switch moyaError {
                case .underlying(let error, _):
                    throw error
                default:
                    throw NSError(domain: Domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription])
                }
            } else {
                throw error
            }
        }
        throw defaultAPIError
    }
    
    func mapArray<T: Mappable>(path:String? = nil) throws -> [T] {
        do {
            let filtedResponce = try self.filterSuccessfulStatusCodes()
            let json = try filtedResponce.mapJSON(failsOnEmptyData: false)
            var jsonToMap = json
            if let path = path {
                if let objectJson = (json as AnyObject).value(forKeyPath: path) {
                    jsonToMap = objectJson
                }
            }
            guard let map = jsonToMap as? [[String:Any]] else {
                throw NSError(domain: Domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey:HTTPURLResponse.localizedString(forStatusCode: statusCode)])
            }
            let object = Mapper<T>().mapArray(JSONArray: map)
            return object
        }
        catch {
            if let moyaError = error as? MoyaError {
                switch moyaError {
                case .underlying(let error, _):
                    throw error
                default:
                    throw NSError(domain: Domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription])
                }
            } else {
                throw error
            }
        }
        throw defaultAPIError
    }
}
