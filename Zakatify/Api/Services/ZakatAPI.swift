//
//  ZakatAPI.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let ZakatAPIProvider = MoyaProvider<ZakatAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                          responseDataFormatter: JSONResponseDataFormatter),
                                                      AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

enum ZakatAPI {
    case add(username:String, json:[String:Any])
    case update(username:String,json:[String:Any])
    case get(username:String)
    case delete(username:String)
}

extension ZakatAPI: TargetType {
    public var headers: [String : String]? {
        return nil
    }

    public var baseURL: URL { return URL(string: "https://flash-bff-ios.appspot.com/api/v1/preference/user-zakat-goal")! }
    public var path: String {
        switch self {
        case .add(username: let username, json: _):
            return "/\(username)"
        case .update(username: let username, json: _):
            return "/\(username)"
        case .get(username: let username):
            return "/\(username)"
        case .delete(username: let username):
            return "/\(username)"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .add:
            return .post
        case .update:
            return .put
        case .get:
            return .get
        case .delete:
            return .put
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .add(username:_, json: let json):
            return json
        case .update(username: _, json: let json):
            return json
        case .get(username:_):
            return nil
        case .delete(username:_):
            return nil
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        if let parameters = self.parameters {
            return .requestParameters(parameters: parameters, encoding: self.parameterEncoding)
        }
        return .requestPlain
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
        return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}
