//
//  UserServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result

protocol ChatServices {
    func getRoomID(username: String, otherUsername: String, complete:@escaping (_ result:Result<Room,NSError>)-> Void)
    func getRooms(username: String, complete:@escaping (_ result:Result<[Room],NSError>)-> Void)
    func push(username: String, receiver: String, connectionID: String, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
}


class ChatServicesCenter: ChatServices {
    func getRoomID(username: String, otherUsername: String, complete: @escaping (Result<Room, NSError>) -> Void) {
        chatAPIProvider.request(.getRoomID(username: username, username: otherUsername)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let room:Room = try response.mapObject()
                    complete(Result.success(room))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    func getRooms(username: String, complete: @escaping (Result<[Room], NSError>) -> Void) {
        chatAPIProvider.request(.messageList(username: username)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let rooms:[Room] = try response.mapArray(path: "messages")
                    complete(Result.success(rooms))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    func push(username: String, receiver: String, connectionID: String, complete: @escaping (Result<Bool, NSError>) -> Void) {
        chatAPIProvider.request(.push(username: username, receiver: receiver,connectionId: connectionID)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let _ = try response.filterSuccessfulStatusCodes()
                    complete(Result.success(true))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}
