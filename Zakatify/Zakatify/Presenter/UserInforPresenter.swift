//
//  UserInforPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit
import FluidValidator
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookLogin
import TwitterKit
import Cloudinary

private class UserValidator: AbstractValidator<UserInfo> {
    override init() {
        super.init()
        self.addValidation(withName: "username") { (user) -> (Any?) in
            user.username
            }.addRule(Length(min: 3, max: 20))
        self.addValidation(withName: "email") { (user) -> (Any?) in
            user.email
            }.addRule(ValidEmail())
    }
}
protocol UserInfoView: CommonView {
    func fillData()
    func saveSuccess()
}

protocol UserInfoViewPresenter: class {
    var view: UserInfoView {get}
    var user: UserInfo {get}
    var photoUrl:URL? {get}
    var userRateValue: Float {get}
    var userRate: String {get}
    var userFullName: String {get}
    var username: String {get}
    var userEmail: String {get}
    var currentMoney: String {get}
    var totolMoney: String {get}
    var userRank: String {get}
    var userPoints: String {get}
    
    var userSelectedTags: [Tag] {get set}
    var editPhoto: UIImage? {get set}
    var userFirstName: String {get set}
    var userLastName: String {get set}
    var userMobile: String {get set}
    var userLocation: String {get set}
    
    var userFacebookEmail: String {get set}
    var userTwitterEmail: String {get set}
    
    init(view: UserInfoView, model:UserInfo)
    func refresh()
    func saveChange()
}

class UserInfoPresenter: UserInfoViewPresenter {
    let view: UserInfoView
    var user: UserInfo
    
    let service: UserServices = UserServicesCenter()

    var photoUrl: URL? {
        return user.photoUrlLink
    }
    
    var userRateValue: Float {
        return Float(user.progess)
    }

    var userRate: String {
        return "\(userRateValue)%"
    }
    
    var userFullName: String {
        return "\(user.firstName) \(user.lastName)"
    }
    
    var username: String {
        return user.username
    }
    
    var userEmail: String {
        return user.email
    }
    
    var currentMoney: String {
        return "$\(Int(user.current))"
    }
    
    var totolMoney: String {
        return "$\(Int(user.goal))"
    }
    var userRank: String {
        return "Rank 13"
    }
    var userPoints: String {
        return "Points 2532"
    }
    
    var userSelectedTags: [Tag] {
        get {
            return user.tags
        }
        set {
            user.tags = newValue
        }
    }
    
    var editPhoto: UIImage? {
        didSet {
            self.view.fillData()
        }
    }
    
    var userFirstName: String {
        get {
            return user.firstName
        }
        set {
            user.firstName = newValue
        }
    }
    var userLastName: String {
        get {
            return user.lastName
        }
        set {
            user.lastName = newValue
        }
    }
    var userMobile: String {
        get {
            return user.mobile
        }
        set {
            user.mobile = newValue.removeSpace()
        }
    }
    var userLocation: String {
        get {
            return user.location
        }
        set {
            user.location = newValue
        }
    }
    
    var userFacebookEmail: String {
        get {
            return user.facebookEmail
        }
        set {
            user.facebookEmail = newValue
        }
    }
    
    var userTwitterEmail: String {
        get {
            return user.twitterEmail
        }
        set {
            user.twitterEmail = newValue
        }
    }
    
    required init(view: UserInfoView, model:UserInfo) {
        self.view = view
        self.user = model
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: UserManager.Notification.currentUserChanged.rawValue), object: nil, queue: nil) { [unowned self] (notification) in
            if let user = UserManager.shared.currentUser {
                self.user = user
                self.view.fillData()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refresh() {
        
    }
    
    func saveChange() {
        if let _ = editPhoto {
            self.uploadImage()
            return
        }

        view.showLoading()
        service.update(user, image: nil) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                UserManager.shared.currentUser = user
                strongSelf.view.showAlert("Save Successful")
                strongSelf.view.saveSuccess()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    
    func  uploadImage() {
        let cloudinary = AppDelegate.shareInstance().cloudinary
        guard let image = self.editPhoto else {
            return
        }
        guard let data = UIImageJPEGRepresentation(image, 0.5) else {
            return
        }
        view.showLoading()
        cloudinary.createUploader().signedUpload(data: data, params: nil, progress: { (progess) in
            print(progess)
        }) { [weak self] (result, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else {
                if let url = result?.url {
                    strongSelf.user.profileUrl = url
                    strongSelf.editPhoto = nil
                    strongSelf.saveChange()
                } else {
                    strongSelf.view.showAlert("Upload image fail, please retry")
                }
            }
        }
    }
}

extension UserInfoViewPresenter {
    func loginFacebook() {
        if let _ = FBSDKAccessToken.current() {
            getFacebookEmail()
            return
        }
        let manager = FBSDKLoginManager()
        manager.logIn(withReadPermissions: ["public_profile","email"], from: view as! UIViewController) { [weak self] (result, error) in
            guard let strongSelf = self else {
                return
            }
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else if let result = result {
                if result.isCancelled {
                    
                } else {
                    strongSelf.getFacebookEmail()
                }
            }
        }
    }
    
    func getFacebookEmail() {
        guard let _ = FBSDKAccessToken.current() else {
            return
        }
        let params = ["fields":"id,name,email"]
        view.showLoading()
        FBSDKGraphRequest(graphPath: "me", parameters: params).start { [weak self] (connection, result, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else if let result = result as? [String:Any] {
                if let email = result["email"] as? String {
                    strongSelf.userFacebookEmail = email
                    strongSelf.view.fillData()
                }
            }
        }
    }
    
    func loginTwitter() {
        let client = TWTRAPIClient.withCurrentUser()
        if let _ = client.userID {
            getTwitterEmail()
            return
        }
        Twitter.sharedInstance().logIn(completion: {[weak self] (session, error) in
            guard let strongSelf = self else {
                return
            }
            if (session != nil) {
                strongSelf.getTwitterEmail()
            } else if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            }
        })
    }
    
    func getTwitterEmail() {
        let client = TWTRAPIClient.withCurrentUser()
        view.showLoading()
        client.requestEmail { [weak self] email, error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            
            if let email = email {
                strongSelf.userTwitterEmail = email
                strongSelf.view.fillData()
            } else if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
}
