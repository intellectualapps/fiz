//
//  ListViewPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

public protocol TableViewPresenter {
    func numberOfSections() -> Int
    func numberOfRowsInSection(section:Int) -> Int
    func refresh()
    func refresh(search: String?)
    func loadmore()
    func shouldLoadmore() -> Bool
}

extension TableViewPresenter {
    func numberOfSections() -> Int {
        return 1
    }
    func numberOfRowsInSection(section:Int) -> Int {
        return 0
    }
}

public protocol TableView: CommonView {
    var refTableView: UITableView? {get}
    var refPresenter: TableViewPresenter? {get}
    func loadingMore()
    func loadedMore()
    func insert(indexPaths: [IndexPath])
    func refreshing()
    func refreshed()
}

extension TableView where Self: UITableViewController {
    var refTableView: UITableView? {
        return self.tableView
    }
}

extension TableView {
    func insert(indexPaths: [IndexPath]) {
        guard let tableview = self.refTableView else {
            return
        }
        tableview.beginUpdates()
        tableview.insertRows(at: indexPaths, with: UITableViewRowAnimation.automatic)
        tableview.endUpdates()
    }
    
    func loadingMore() {
        guard let tableView = refTableView else {
            return
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        indicator.center = CGPoint(x: tableView.frame.size.width/2.0, y: 15)
        indicator.startAnimating()
        view.addSubview(indicator)
        refTableView?.tableFooterView = view
    }
    
    func loadedMore() {
        guard let tableView = refTableView else {
            return
        }
        tableView.reloadData()
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 2))
        tableView.tableFooterView = view
    }
    
    func refreshing() {
        refTableView?.refreshControl?.beginRefreshing()
    }
    
    func refreshed() {
        refTableView?.refreshControl?.endRefreshing()
        guard let tableview = self.refTableView else {
            return
        }
        tableview.reloadData()
    }
}


