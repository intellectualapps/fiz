//
//  UpdateUserInfoPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import TwitterKit
import FluidValidator
import Cloudinary

protocol UpdateUserInfoView: CommonView {
    func updateSocicalProfile()
    func saveSuccess()
}

protocol UpdateUserInfoViewPresenter: class {
    var user:UserInfo {get set}
    init(view:UpdateUserInfoView, model:UserInfo)
    func getSocialProfile()
    func getFacebookProfile()
    func getTwitterProfile()
    func saveChange()
}

private class UserValidator: AbstractValidator<UserInfo> {
    override init() {
        super.init()
        self.addValidation(withName: "username") { (user) -> (Any?) in
            user.username
            }.addRule(Length(min: 3, max: 20))
        self.addValidation(withName: "email") { (user) -> (Any?) in
            user.email
            }.addRule(ValidEmail())
    }
}

class UpdateUserInfoPresenter: UpdateUserInfoViewPresenter {
    
    var user: UserInfo
    let view: UpdateUserInfoView
    let service: UserServices = UserServicesCenter()
    
    var userFullName: String {
        return "\(user.firstName) \(user.lastName)"
    }
    
    var usersocialName: String {
        if (FBSDKAccessToken.current() != nil) {
            return user.facebookCompleteName
        } else if (TWTRAPIClient.withCurrentUser().userID != nil) {
            return user.twitterName
        }
        return ""
    }
    
    var usersocialFullName: String {
        if (FBSDKAccessToken.current() != nil) {
            return user.facebookCompleteName
        } else if (TWTRAPIClient.withCurrentUser().userID != nil) {
            return user.twitterName
        }
        return ""
    }
    
    var username: String {
        set {
            user.username = newValue
        }
        get {
            return user.username
        }
    }
    
    var userEmail: String {
        set {
            user.email = newValue
        }
        get {
            return user.email
        }
    }
    var socialEmail: String {
        if (FBSDKAccessToken.current() != nil) {
            return user.facebookEmail
        } else if (TWTRAPIClient.withCurrentUser().userID != nil) {
            return user.twitterEmail
        }
        return ""
    }
    
    var socialPhotoUrl: String = ""
    
    required init(view: UpdateUserInfoView, model: UserInfo) {
        self.view = view
        self.user = model
    }
    
    func getSocialProfile() {
        if (FBSDKAccessToken.current() != nil) {
            getFacebookProfile()
        } else if (TWTRAPIClient.withCurrentUser().userID != nil) {
            getTwitterProfile()
        }
    }
    
    func getFacebookProfile() {
        view.showLoading()
        FBSDKProfile.loadCurrentProfile {[weak self] (profile, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            guard let profile = FBSDKProfile.current() else {
                if let error = error {
                    strongSelf.view.showAlert(error.localizedDescription)
                } else {
                    
                }
                return
            }
            strongSelf.user.firstName = profile.firstName
            strongSelf.user.lastName = profile.lastName
            strongSelf.socialPhotoUrl = profile.imageURL(for: FBSDKProfilePictureMode.square, size: CGSize(width: 500, height: 500)).absoluteString
            strongSelf.user.facebookCompleteName = profile.name
            strongSelf.view.updateSocicalProfile()
        }
    }
    
    func getTwitterProfile() {
        let client = TWTRAPIClient.withCurrentUser()
        if let userID = client.userID {
            view.showLoading()
            client.loadUser(withID: userID) { [weak self] (twUser:TWTRUser?, error:Error?) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.view.hideLoading()
                guard let twUser = twUser else {
                    if let error = error {
                        strongSelf.view.showAlert(error.localizedDescription)
                    } else {
                        
                    }
                    return
                }
                strongSelf.user.twitterUsername = twUser.name
                strongSelf.user.twitterName = twUser.screenName
                strongSelf.socialPhotoUrl = twUser.profileImageURL
                strongSelf.view.updateSocicalProfile()
            }
        }
    }
    
    func saveChange() {
        if user.profileUrl.isEmpty, socialPhotoUrl.isEmpty == false {
            uploadPhoto(url: socialPhotoUrl)
            return
        }
        if UserInfo.userValidator.validate(value: self.username) == false {
            self.view.showAlert(UserInfo.userValidator.errorMessage())
            return
        }
        if UserInfo.emailValidator.validate(value: self.userEmail) == false {
            self.view.showAlert(UserInfo.emailValidator.errorMessage())
            return
        }
        view.showLoading()
        service.createUser(user, tempKey: user.tempKey ?? "", complete: { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                strongSelf.user.username = user.username
                strongSelf.user.authToken = user.authToken
                UserManager.shared.currentUser = strongSelf.user
                UserManager.shared.authenToken = user.authToken
                strongSelf.view.saveSuccess()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        })
    }
    
    func uploadPhoto(url:String) {
        let cloudinary = AppDelegate.shareInstance().cloudinary
        guard let url = URL(string: url) else {
            return
        }
        view.showLoading()
        cloudinary.createUploader().signedUpload(url: url, params: nil, progress: { (progess) in
            print(progess)
        }) { [weak self] (result, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else {
                if let url = result?.url {
                    strongSelf.user.profileUrl = url
                    strongSelf.saveChange()
                } else {
                    strongSelf.view.showAlert("Upload image fail, please retry")
                }
            }
        }
    }
    
}
