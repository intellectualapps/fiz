//
//  AlertView.swift
//  Zakatify
//
//  Created by Thang Truong on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit


class AlertView: UIVisualEffectView {
    static let shared : AlertView? = {
        if let viewArray  =  Bundle.main.loadNibNamed("AlertView", owner: nil, options: nil) {
            for item in viewArray {
                if item is AlertView {
                    return item as? AlertView
                }
            }
        }
        return nil
    }()
    let height: CGFloat = 94
    let barStatusHeight: CGFloat = 20
    var topConstraint : NSLayoutConstraint!
    
    var isShowing = true {
        didSet {
            guard isShowing != oldValue else {return}
            UIView.animate(withDuration: 0.35, delay: 0, options: .curveEaseInOut, animations: {
                self.topConstraint.constant = self.isShowing ?  self.barStatusHeight : -self.height - self.barStatusHeight
                self.superview?.layoutIfNeeded()
            }) { success in
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        guard let window = AppDelegate.shareInstance().window else {return}
        window.addSubview(self)
        let margins = window.layoutMarginsGuide
        topConstraint = self.topAnchor.constraint(equalTo: window.topAnchor, constant: barStatusHeight)
        topConstraint.isActive = true
        self.leftAnchor.constraint(equalTo: margins.leftAnchor).isActive = true
        self.rightAnchor.constraint(equalTo: margins.rightAnchor).isActive = true
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        isShowing = false
    }
    
   
    
    
    func toggle() {
        isShowing = !isShowing
    }
    
    

}
