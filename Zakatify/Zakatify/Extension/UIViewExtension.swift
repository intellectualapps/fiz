//
//  UIViewExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func removeAllSubviews() {
        subviews.forEach { (view) in
            view.removeAllSubviews()
        }
        self.removeFromSuperview()
    }

    class func fromNib<T: UIView>(nibNameOrNil: String? = nil) -> T {
        let v: T? = fromNib(nibNameOrNil: nibNameOrNil)
        return v!
    }

    //Initialize instance from nib file
    class func fromNib<T: UIView>(nibNameOrNil: String? = nil) -> T? {
        var view: T?
        var name = ""
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            name = "\(T.self)".components(separatedBy: ".").last ?? ""
        }
        if let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil) {
            for v in nibViews {
                if let tog = v as? T {
                    view = tog
                }
            }
        }
        return view
    }
    
    func constraintFor(attribute: NSLayoutAttribute) -> NSLayoutConstraint? {
        var listConstraint = [NSLayoutConstraint]()
        if attribute == .height || attribute == .width {
            listConstraint = self.constraints
        } else {
            if let constraints = self.superview?.constraints {
                listConstraint = constraints
            }
        }
        for constraint in listConstraint {
            if constraint.firstAttribute == attribute {
                return constraint
            }
        }
        return nil
    }
    
    func addDashedBorder(color:UIColor, width:Float, space:Float) {
        let color = color.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [NSNumber(value:width), NSNumber(value:space)]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: frameSize.height/2).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    func traverseSubviewForViewOfKind(kind: AnyClass) -> UIView? {
        var matchingView: UIView?
        
        for aSubview in subviews {
            if type(of: aSubview) == kind {
                matchingView = aSubview
                return matchingView
            } else {
                if let matchingView = aSubview.traverseSubviewForViewOfKind(kind: kind) {
                    return matchingView
                }
            }
        }
        return matchingView
    }
}


