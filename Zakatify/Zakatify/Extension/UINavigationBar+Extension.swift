//
//  UINavigationBar+Extension.swift
//  Mosaic
//
//  Created by truong.tuan.quang on 5/23/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.barTintColor = UIColor.navigationBartinColor
        self.tintColor = UIColor.navigationTintColor
    }
}
