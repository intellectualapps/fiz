//
//  UIViewControllerExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

extension UIViewController {
    func removeFromParentRecursive() {
        print(NSStringFromClass(self.classForCoder) + "." + #function)

        if let current = self as? UINavigationController {
            current.viewControllers.forEach { (controller) in
                controller.removeFromParentRecursive()
            }
            current.viewControllers = []
        } else {
            for controller in childViewControllers {
                controller.removeFromParentRecursive()
            }
        }
        self.willMove(toParentViewController: nil)
        self.removeFromParentViewController()
        self.view.removeAllSubviews()
    }

    class func isShowingAlertController(title: String, message: String) -> Bool {
        guard let rootController = UIApplication.shared.keyWindow?.rootViewController else {
            return false
        }
        if let presentedController = UIViewController.topViewControllerWithRootController(rootController) as? UIAlertController {
            if presentedController.title == title && presentedController.message == message {
                return true
            }
        }
        return false
    }
    
    public class func topMostViewController() -> UIViewController? {
        let keyWindow = UIApplication.shared.keyWindow
        return UIViewController.topViewControllerWithRootController(keyWindow?.rootViewController)
    }

    public class func topViewControllerWithRootController(_ root: UIViewController?) -> UIViewController? {
        guard let rootController = root else {
            return nil
        }
        if let tabbarController = rootController as? UITabBarController {
            return UIViewController.topViewControllerWithRootController(tabbarController.selectedViewController)
        } else if let naviagationController = rootController as? UINavigationController {
            return UIViewController.topViewControllerWithRootController(naviagationController.visibleViewController)
        } else if let presentedController = rootController.presentedViewController {
            return UIViewController.topViewControllerWithRootController(presentedController)
        } else {
            for view in rootController.view.subviews {
                if let controller = view.next as? UIViewController {
                    return UIViewController.topViewControllerWithRootController(controller)
                }
            }
        }
        return root
    }

    public func addBackButtonDefault() {
        let leftBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back-indicator"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.back))
        navigationItem.leftBarButtonItem = leftBtn
    }
    
    func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    public func addLeftCloseButton() {
        let leftBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "ic-close"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.close))
        navigationItem.leftBarButtonItem = leftBtn
    }
    
    func close() {
        self.dismiss(animated: true, completion: nil)
    }

    public func addLeftBarButtonWithImage(buttonImage: UIImage, tintColor: UIColor = UIColor(hex: 0xB4B4B4)) {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        button.setImage(buttonImage, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: -12, left: 0, bottom: 12, right: 0)
        button.addTarget(self, action: #selector(self.toggleLeft), for: UIControlEvents.touchUpInside)
        let leftBtn = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = leftBtn
    }

    public func addRightBarButtonWithImage(buttonImage: UIImage, tintColor: UIColor? = nil) {
        let rightButton = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(UIViewController.toggleRight))
        rightButton.tintColor = tintColor
        navigationItem.rightBarButtonItem = rightButton;
    }

    func addRightBarButtonWithTitle(title: String) {
        let font = UIFont.systemFont(ofSize: 16)
        let size = title.sizeWithBoundSize(boundSize: CGSize(width: 200, height: 40), option: NSStringDrawingOptions.usesLineFragmentOrigin, font: font)
        let rightBtn  = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: size.width  + 10, height: size.height))
        rightBtn.titleLabel?.textAlignment = NSTextAlignment.right
        rightBtn.setTitle(title, for: UIControlState.normal)
        rightBtn.addTarget(self, action: #selector(UIViewController.toggleRight), for: UIControlEvents.touchUpInside)
        let rightButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn)
        navigationItem.rightBarButtonItem = rightButton;
    }

    func toggleLeft() {
        
    }

    func toggleRight() {
    }
    
    // MARK: - Config naviagation appearend
    
    func setupNavigationDefault() {
        self.setupNavigationAppearend(backgroundImage: UIImage(),
                                      shadowImage: UIImage(),
                                      hidden: false,
                                      translucent: true,
                                      backgroundColor: UIColor.clear,
                                      tintColor: UIColor.white,
                                      statusBarStyle: UIStatusBarStyle.lightContent)
    }
    
    func setupNavigationAppearend(backgroundImage: UIImage = UIImage(), shadowImage: UIImage = UIImage(), hidden: Bool = false, translucent: Bool = true, backgroundColor: UIColor = UIColor.clear, tintColor: UIColor = UIColor.white, barStyle: UIBarStyle = UIBarStyle.black, statusBarStyle: UIStatusBarStyle = UIStatusBarStyle.lightContent) {
        self.navigationController?.navigationBar.setBackgroundImage(backgroundImage, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = shadowImage
        self.navigationController?.navigationBar.isHidden = hidden
        self.navigationController?.navigationBar.isTranslucent = translucent
        self.navigationController?.navigationBar.backgroundColor = backgroundColor
        self.navigationController?.view.backgroundColor = backgroundColor
        self.navigationController?.navigationBar.barStyle = barStyle
        self.navigationController?.navigationBar.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor(hex: 0xffffff, alpha: 0.2), thickness: 1.0)
        self.navigationController?.navigationBar.setTitleVerticalPositionAdjustment(CGFloat(-12), for: UIBarMetrics.default)
        UIApplication.shared.statusBarStyle = statusBarStyle
        UINavigationBar.appearance().tintColor = tintColor
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func setStateStatusBar(hidden: Bool) {
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func setCustomTitleNavigation(title: String? = nil, image: UIImage? = nil) {
        if let img = image {
            let imageView = UIImageView(image: img)
            imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            imageView.contentMode = .scaleAspectFit
            self.navigationItem.titleView = imageView
        } else {
            self.title = title
        }
    }

    func childControllerWithType(aclass: AnyClass) -> UIViewController? {
        for controller in self.childViewControllers {
            if controller.classForCoder == aclass {
                return controller
            }
        }
        return nil
    }
    
    func openWebview(type: WebviewUrl, title: String ) {
        if let vc = UIStoryboard.webview().instantiateViewController(withIdentifier: "WebviewController") as? WebviewController {
            vc.title = title
            vc.url_redirect = type.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func openWebview(url: URL, title: String) {
        if let vc = UIStoryboard.webview().instantiateViewController(withIdentifier: "WebviewController") as? WebviewController {
            vc.title = title
            vc.url_redirect = url.absoluteString
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func addSwipeLeft() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(backOrGotoHomeScreen))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func addSwipeRight() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(backOrGotoHomeScreen))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func backOrGotoHomeScreen() {
         self.back()
    }
}

extension UIViewController: MFMailComposeViewControllerDelegate {
    func presentEmailVC(recipients: [String], subject: String = "", body: String = "") {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients(recipients)
        composeVC.setSubject(subject)
        composeVC.setMessageBody(body, isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
