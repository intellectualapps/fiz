//
//  ImageExtensions.swift
//  Pods
//
//  Created by Kevin Belter on 1/2/17.
//
//

import Foundation
import SDWebImage

extension UIImageView {
    func setImageWithURLAnimated(_ anURL:String, completitionBlock:(() -> ())? = nil) {
        let image = UIImage(color: UIColor.gray)
        if let url = URL(string: anURL) {
            sd_setImage(with: url, placeholderImage: image, options: SDWebImageOptions()) { (_, _, _, _) in
                completitionBlock?()
            }
        } else {
            self.image = image
        }
    }
}

extension UIImage {
    func resizeWith(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    var jpegRepresentation:Data? {
        return UIImageJPEGRepresentation(self, 0.8)
    }
}
