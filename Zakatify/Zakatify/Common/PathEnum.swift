//
//  PathEnum.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
private struct Constants {
    static let baseUrl = Enviroment.env(dev: "https://flash-bff-ios.appspot.com/api/v1/",
                                        stg: "https://flash-bff-ios.appspot.com/api/v1/",
                                        prod: "https://flash-bff-ios.appspot.com/api/v1/")
}

enum Path {
    case Login
    case loginFacebook
    case getPaypalAccounts
    case charitySearch
    case zakatifierSearch
    case charityList(String)
    case chartityReview(Int)
    case charityDetail(Int)
    case paypalDelete(String)
    case paypalGet(String)
    case paypalSetPrimary(String)
    
    var relativePath: String {
        switch self {
        case .getPaypalAccounts:
            return "preference/paypal-accounts"
        case .Login:
            return ""
        case .charitySearch:
            return "charity/search"
        case .zakatifierSearch:
            return "zakatifier/search"
        case .charityList(let username):
            return "charity/suggest/\(username)"
        case .chartityReview(let id):
            return "review/charity/\(id)"
        case .charityDetail(let id):
            return "charity/details/\(id)"
        case .paypalDelete(let id):
            return "payment/remove/\(id)"
        case .paypalGet(let username):
            return "preference/user-payment-option/\(username)"
        case .paypalSetPrimary(let id):
            return "preference/user-payment-option/primary/\(id)"
        case .loginFacebook:
            return "user/authenticate"
        }
    }

    var path: String {
        return (NSURL(string: Constants.baseUrl)?.appendingPathComponent(relativePath)?.absoluteString) ?? ""
    }
}
