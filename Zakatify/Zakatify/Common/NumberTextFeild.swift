//
//  NumberTextFeild.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift

class NumberTextFeild: MaterialTextFeild {
    override var text: String? {
        set {
            super.text = newValue
            self.formatText()
        }
        get {
            return super.text
        }
    }
    
    let formatter = NumberFormatter()
    var number: NSNumber? {
        guard let text = self.text else {
            return nil
        }
        let textReplace = text.replacingOccurrences(of: ",", with: "")
        if let number = formatter.number(from: textReplace) {
            return number
        }
        return nil
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        self.keyboardType = .numberPad
        formatter.numberStyle = NumberFormatter.Style.decimal

        self.rx.text.bind { [unowned self] (text) in
            self.formatText()
        }.addDisposableTo(disposeBag)
    }
    
    func formatText() {
        guard let text = super.text else {
            return
        }
        let textReplace = text.replacingOccurrences(of: ",", with: "")
        if let number = formatter.number(from: textReplace) {
            super.text = formatter.string(from: number)
        }
    }

}
