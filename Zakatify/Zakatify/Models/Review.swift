//
//  Review.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

protocol Review {
    var username: String {get set}
    var firstName: String {get set}
    var lastName: String {get set}
    var charityId: Int {get set}
    var charityName: String {get set}
    var rate: Int {get set}
    var id: Int {get set}
    var comment: String {get set}
    var photoUrl: String {get set}
    var createDate: Date? {get set}
}

class CharityReview: Review, Mappable {
    var username: String  = ""
    var firstName: String  = ""
    var lastName: String = ""
    var charityId: Int = 0
    var charityName: String = ""
    var rate: Int = 0
    var id: Int = 0
    var comment: String = ""
    var photoUrl: String = ""
    var createDate: Date?
    enum JSONKey: String {
        case username
        case firstName
        case lastName
        case charityId
        case charityName = "charity"
        case rate = "rating"
        case id = "reviewId"
        case comment
        case photoUrl
        case createDate = "createdOn"
    }

    var tranform: DateFormatterTransform {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let tranform = DateFormatterTransform(dateFormatter: dateFormatter)
        return tranform
    }

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        id <- map[JSONKey.id.rawValue]
        username <- map[JSONKey.username.rawValue]
        firstName <- map[JSONKey.firstName.rawValue]
        lastName <- map[JSONKey.lastName.rawValue]
        charityId <- map[JSONKey.charityId.rawValue]
        charityName <- map[JSONKey.charityName.rawValue]
        rate <- map[JSONKey.rate.rawValue]
        comment <- map[JSONKey.comment.rawValue]
        photoUrl <- map[JSONKey.photoUrl.rawValue]
        if let datestr = map.JSON["createdOn"] as? String {
            createDate = Formatter.iso8601.date(from: datestr)
        }
    }
}
