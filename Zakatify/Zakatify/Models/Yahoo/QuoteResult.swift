//
//  QuoteResult.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/24/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

class StockSearchResult: Model {
    var symbol: String?
    var name: String?
    var exchange: String?
    var assetType: String?

    override func parseContentFromDict(dict: [String : Any]) {
        symbol = dict.stringForKey(key: "symbol")
        name = dict.stringForKey(key: "name")
        exchange = dict.stringForKey(key: "exchDisp")
        assetType = dict.stringForKey(key: "typeDisp")
    }
}
