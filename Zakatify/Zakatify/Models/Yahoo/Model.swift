//
//  Model.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/24/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

class Model: NSObject {
    override init() {
        super.init()
    }

    convenience init(dict: [String: Any]) {
        self.init()
        parseContentFromDict(dict: dict)
    }

    func parseContentFromDict(dict: [String: Any]) {

    }
}
