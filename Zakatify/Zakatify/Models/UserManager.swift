//
//  UserManager.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class UserManager {
    enum Notification: String {
        case currentUserChanged
    }
    
    static let shared = UserManager()
    enum Key:String {
        case savedUser
        case savedAuthenToken
    }
    var currentUser:UserInfo? {
        set {
            let json = newValue?.toSaveJSON()
            UserDefaults.standard.set(json, forKey: Key.savedUser.rawValue)
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.currentUserChanged.rawValue), object: nil)
            if let user = currentUser {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        
        get {
            if let json = UserDefaults.standard.object(forKey: Key.savedUser.rawValue) as? [String:Any] {
                let user = Mapper<UserInfo>().map(JSON: json)
                return user
            }
            return nil
        }
    }
    
    var authenToken: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: Key.savedAuthenToken.rawValue)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: Key.savedAuthenToken.rawValue) as? String
        }
    }
    
    func logout() {
        self.currentUser = nil
        self.authenToken = nil
        AppDelegate.shareInstance().gotoLoginViewController()
    }
}
