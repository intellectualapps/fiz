//
//  ZakatAPI.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let InviteAPIProvider = MoyaProvider<InviteAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                          responseDataFormatter: JSONResponseDataFormatter),
                                                      AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

enum InviteAPI {
    case userInvitations(username: String, page: Int, size: Int)
    case sendInvitation(username: String, inviteeEmail: String)
}

extension InviteAPI: TargetType {
    public var headers: [String : String]? {
        return nil
    }

    public var baseURL: URL { return URL(string: "https://flash-bff-ios.appspot.com/api/v1/invitation")! }
    public var path: String {
        switch self {
        case .userInvitations(username: let username, page: _, size: _):
            return "/\(username)"
        case .sendInvitation(username: let username, inviteeEmail: let inviteeEmail):
            return "/"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .userInvitations:
            return .get
        case .sendInvitation:
            return .post
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .userInvitations(username: let _, page: let page, size: let size):
            return ["page-size": size, "page-number": page]
        case .sendInvitation(username: let username, inviteeEmail: let inviteeEmail):
            return ["invitor-username": username, "invitee-email": inviteeEmail]
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        if let parameters = self.parameters {
            return .requestParameters(parameters: parameters, encoding: self.parameterEncoding)
        }
        return .requestPlain
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
        return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}
