//
//  ZakatifierSearchServices.swift
//  Zakatify
//
//  Created by MTQ on 7/27/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class ZakatifierSearchServices: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var zakatifiers: [UserPublicProfile] = []

        if let root = response as? [String: Any] {
            if let zakatifiersDict = root["zakatifiers"] as? [[String: Any]] {
                for zakatifierDict in zakatifiersDict {
                    if let zakatifier = UserPublicProfile(JSON: zakatifierDict) {
                        zakatifiers.append(zakatifier)
                    }
                }
            }
        }
        super.onFinish(zakatifiers, error: error, completion: completion)
    }
}
