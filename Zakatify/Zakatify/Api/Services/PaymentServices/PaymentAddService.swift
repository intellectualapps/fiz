//
//  PaymentAddService.swift
//  Zakatify
//
//  Created by center12 on 6/11/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
class PaymentAddService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var state: Bool?
        if let root = response as? [String: Any] {
            state = root.boolForKey(key: "state")
        }
        super.onFinish(state, error: error, completion: completion)
    }
}
