//
//  CharityReviewService.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
class CharityReviewService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results: CharityReviewInfo?
        if let root = response as? [String: Any] {
            results = CharityReviewInfo(dict: root)
        }
        super.onFinish(results, error: error, completion: completion)
    }
}
