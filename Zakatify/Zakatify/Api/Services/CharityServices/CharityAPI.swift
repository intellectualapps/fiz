//
//  ZakatAPI.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let CharityAPIProvider = MoyaProvider<CharityAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                          responseDataFormatter: JSONResponseDataFormatter),
                                                      AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

enum CharityAPI {
    case suggestList(user:String, page: Int, size: Int)
    case search(user:String, query: String, page: Int, size: Int)
    case add(user:String, id: Int)
    case remove(user:String, id: Int)
    case userPortfolio(user:String, page: Int, size: Int)
    case charityDetails(id: Int, user: String)
    case charityReviews(id: Int, page: Int, size: Int)
    case review(id: Int, rate: Int, content: String, username: String)
    case donate(id: Int, amount: Int)
}

extension CharityAPI: TargetType {
    var headers: [String : String]? {
        return nil
    }
    public var baseURL: URL {
        switch self {
        case .add(user: _, id: _), .remove(user: _, id: _), .userPortfolio(user: _, page: _, size: _):
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/preference")!
        case .charityReviews, .review:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/review/charity")!
        case .donate:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/donation")!
        case .search:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/charity/search")!
        default:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/charity")! }
        }
    public var path: String {
        switch self {
        case .suggestList(user: let username, page: _, size: _):
            return "/suggest/\(username)"
        case .search(user: _, query: _, page: _, size: _):
             return ""
        case .add(user: let username, id: _):
            return "/user-charity/\(username)"
        case .remove(user: let username, id: _):
            return "/user-charity/\(username)"
        case .userPortfolio(user: let username, page: _, size: _):
            return "/user-charity/\(username)"
        case .charityDetails(id: let id, let username):
            return "/details/\(id)"
        case .charityReviews(id: let id, page: _, size: _):
            return "/\(id)"
        case .review(id: let id, rate: _, content: _, username: _):
            return "/\(id)"
        case .donate(id: _, amount: _):
            return "/manual/donation-request"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .suggestList(user: _, page: _, size: _):
            return .get
        case .search(user: _, query: _, page: _, size: _):
            return .get
        case .add(user: _, id: _):
            return .post
        case .remove(user: _, id: _):
            return .delete
        case .userPortfolio:
            return .get
        case .charityDetails(id: _):
            return .get
        case .charityReviews(id: _, page: _, size: _):
            return .get
        case .review:
            return .post
        case .donate:
            return .post
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .suggestList(user: _, page: let page, size: let size):
            return ["page-number":page,"page-size":size]
        case .search(user: let user, query: let key, page: let page, size: let size):
            return ["username":user,"query":key,"page-number":page,"page-size":size]
        case .add(user: _, id: let id):
            return ["charity-id":id]
        case .remove(user: _, id: let id):
            return ["charity-id":id]
        case .userPortfolio(user: _, page: let page, size: let size):
            return ["page-number":page,"page-size":size]
        case .charityDetails(id: _, let username):
            return ["username": username]
        case .charityReviews(id: let _, let page, size: let size):
            return ["page-number":page,"page-size":size]
        case .review(id: let _, rate: let rate, content: let content, username: let username):
            return ["comment": content, "username": username, "rating": rate]
        case .donate(id: let id, amount: let amount):
            return ["charity-id" : id, "amount": amount]
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        if let parameters = self.parameters {
            return .requestParameters(parameters: parameters, encoding: self.parameterEncoding)
        }
        return .requestPlain
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
        return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}
