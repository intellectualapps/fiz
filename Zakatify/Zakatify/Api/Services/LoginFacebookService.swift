//
//  LoginFacebookService.swift
//  Zakatify
//
//  Created by MTQ on 8/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class LoginFacebookService: ApiService {
    override func onFinish(_ response: Any?, statusCode: Int, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var userInfo: UserInfo?
        if let root = response as? [String: Any] {
            if let tempUserInfo = UserInfo(JSON: root) {
                userInfo = tempUserInfo
            }
        }
        super.onFinish(userInfo, error: error, completion: completion)
    }
}
