//
//  ProfileDetailsViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/19/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class ProfileDetailsViewController: UITableViewController, ZakatifierDetailsView {

    // MARK: - Properties
    var isReadMore = false
    var presenter: ZakatifierDetailsPresenter? {
        didSet {
            presenter?.refresh()
        }
    }

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: "DonationTableViewCell", bundle: nil), forCellReuseIdentifier: DonationTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: ReviewTableViewCell.reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _ = self.presentingViewController {
            self.addLeftCloseButton()
        } else {
            self.addBackButtonDefault()
            self.addSwipeRight()
        }
    }
    
    // MARK: - Private Methods
    func dataChanged() {
        if let count = presenter?.model.counts.userDonationHistoryNarrative.count {
            isReadMore = count > 3
        }
        tableView.reloadData()
    }

    @objc func readMore() {
        isReadMore = false
        tableView.reloadData()
    }
    
    private func getCharityDetail(id: Int, username: String, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(username, forKey: "username")
        let encoding = Encoding.forMethod(method: .get)
        let path = Path.charityDetail(id).path
        let service = CharityDetailService(apiPath: path, method: .get, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
    
    func goToCharityDetailScreen(charity: Charity) {
        guard let vc = UIStoryboard.charity().instantiateViewController(withIdentifier: "CharityDetailsViewController") as? CharityDetailsViewController else {
            return
        }
        vc.charity = charity
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 1
        case 1:
            if let count = presenter?.model.counts.userDonationHistoryNarrative.count {
                if isReadMore {
                    return 3
                } else {
                    return count
                }
            }
            return 0
        case 2:
            guard let presenter = presenter else {return 0}
            if presenter.reviews.count > 0 {
                if presenter.shouldLoadmoreReviews() {
                    return presenter.reviews.count + 1
                } else {
                    return presenter.reviews.count
                }
            }
            return 0
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else if (section == 1) {
            return 30
        } else if (section == 2) {
            return 30
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else if (section == 1) {
            if presenter?.model.counts.userDonationHistoryNarrative.count == 0 {
                return 30
            }
            if isReadMore {
                return 30
            }
            return 0
        } else if (section == 2) {
            guard let presenter = presenter else {return 30}
            if presenter.reviews.count == 0 {
                return 30
            }
            return 0
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = UIView.loadFromNibNamed(nibNamed: "TableViewSectionHeader") as? TableViewSectionHeader else {
            return nil
        }
        
        if section == 0 {
            return nil
        } else if (section == 1) {
            view.lb_title.text = "LATEST DONATIONS"
        } else if (section == 2) {
            view.lb_title.text = "LATEST REVIEWS"
        } else {
            return nil
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let view = UIView.loadFromNibNamed(nibNamed: "TableViewSectionHeader") as? TableViewSectionHeader else {
            return nil
        }
        
        if section == 0 {
            return nil
        } else if (section == 1) {
            if presenter?.model.counts.userDonationHistoryNarrative.count == 0 {
                view.lb_title.text = "There are no donations"
            }
            if isReadMore {
                if let view = UIView.loadFromNibNamed(nibNamed: "ReadMoreDonationsFooterView") as? ReadMoreDonationsFooterView {
                    view.readMoreButton.addTarget(self, action: #selector(readMore), for: .touchUpInside)
                    if let count = presenter?.model.counts.userDonationHistoryNarrative.count {
                        view.readMoreButton.setTitle("View all \(count) danotaions", for: .normal)
                    }
                    return view
                }
            }
        } else if (section == 2) {
            guard let presenter = presenter else {
                view.lb_title.text = "There are no reviews"
                return view
            }
            if presenter.reviews.count == 0 {
                view.lb_title.text = "There are no reviews"
                return view
            }
            view.lb_title.text =  ""
        } else {
            return nil
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileDetailsHeaderTableViewCell.reuseIdentifier, for: indexPath) as? ProfileDetailsHeaderTableViewCell else {
                return UITableViewCell()
            }
            cell.presenter = presenter
            return cell
        } else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DonationTableViewCell.reuseIdentifier, for: indexPath) as? DonationTableViewCell else {
                return UITableViewCell()
            }
            if let userDonationHistoryNarrative = presenter?.model.counts.userDonationHistoryNarrative {
                let userDonationHistory = userDonationHistoryNarrative[indexPath.row]
                cell.userDonationHistory = userDonationHistory
            }
            return cell
        } else if indexPath.section == 2 {
            guard let presenter = presenter else {return UITableViewCell()}
            if indexPath.row == presenter.reviews.count && presenter.shouldLoadmoreReviews() {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "readmore", for: indexPath) as? UITableViewCell else {
                    return UITableViewCell()
                }
                return cell
            }
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ReviewTableViewCell.reuseIdentifier, for: indexPath) as? ReviewTableViewCell else {
                return UITableViewCell()
            }
            let review = presenter.reviews[indexPath.row]
            let url = presenter.model.user.profileUrl
            review.photoUrl = url
            cell.review = review
            return cell
        }
        return UITableViewCell()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let userDonationHistoryNarrative = presenter?.model.counts.userDonationHistoryNarrative,
                let username = UserManager.shared.currentUser?.username else {
                return
            }
            let userDonationHistory = userDonationHistoryNarrative[indexPath.row]
            view.showLoading()
            getCharityDetail(id: userDonationHistory.charityId, username: username) { [weak self] (response, error) in
                guard let `self` = self else { return }
                self.view.hideLoading()
                if let err = error {
                    self.view.showAlert(err.message)
                } else {
                    if let charity = response as? Charity {
                        self.goToCharityDetailScreen(charity: charity)
                    }
                }
            }
        } else if indexPath.section == 2 {
            guard let presenter = presenter else {return}
            if indexPath.row == presenter.reviews.count {
                presenter.loadmoreReviews()
            }
        }
    }
}
