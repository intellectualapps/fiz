//
//  ZakatifierTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/9/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class ZakatifierTableViewCell: UITableViewCell {
    static let identifier: String = "ZakatifierTableViewCell"
    
    @IBOutlet weak var uv_chart: CircleView!
    @IBOutlet weak var iv_avatar: ImageView!
    
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_point: UILabel!
    
    lazy var bt_add: UIButton = { [unowned self] in
        let bt = UIButton(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        bt.setImage(#imageLiteral(resourceName: "ic-add"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(ZakatifierTableViewCell.clickAdd), for: UIControlEvents.touchUpInside)
        return bt
    }()
    
    lazy var bt_remove: UIButton = { [unowned self] in
        let bt = UIButton(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        bt.setImage(#imageLiteral(resourceName: "ic-check"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(ZakatifierTableViewCell.clickAdd), for: UIControlEvents.touchUpInside)
        return bt
        }()
    
    var zakatifier: UserPublicProfile? {
        didSet {
            guard let zakatifier = zakatifier else {
                presenter = nil
                return
            }
            presenter = ZakatifierDetailsPresenter(view: self, model: zakatifier)
        }
    }
    var presenter:ZakatifierDetailsPresenter? {
        didSet {
            guard let presenter = presenter else {
                clear()
                return
            }
            fillData()
        }
    }
    
    var clickAddBlock:((_ zakatifer:UserPublicProfile?)->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func clickAdd() {
        self.clickAddBlock?(zakatifier)
    }

}

extension ZakatifierTableViewCell: ZakatifierDetailsView {
    func dataChanged() {
        fillData()
    }

    func clear() {
        uv_chart.value = 0
        iv_avatar.image = nil
        lb_name.text = nil
        lb_point.text = nil
        self.accessoryView = nil
    }
    func fillData() {
        guard let presenter = presenter else { return }
        uv_chart.value = CGFloat(presenter.model.donationProgress.percentCompleted)
        if let url = presenter.model.user.avartarURL {
            iv_avatar.sd_setImage(with: url,
                                  placeholderImage: UIImage(named: "noAvatar"))
        }
        lb_name.attributedText = presenter.model.user.nameAttributedString
        let point = presenter.model.donationProgress.points
        if Int(point) == 0 {
            lb_point.text = "No points"
        } else if floor(point) == point {
            lb_point.text = "\(Int(point)) points"
        } else {
            lb_point.text = "\(point) points"
        }
        if presenter.model.user.email == UserManager.shared.currentUser?.email ?? "" {
            self.accessoryView = nil
        } else if presenter.model.isFollowing.state == false {
            self.accessoryView = bt_add
        } else {
            self.accessoryView = bt_remove
        }
    }
}
