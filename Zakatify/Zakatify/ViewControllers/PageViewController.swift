//
//  PageViewController.swift
//  Zakatify
//
//  Created by Thang Truong on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

protocol PageViewToContainerProtocol: class {
    func pressTabButton()
}

class PageViewController: UIPageViewController, UIPageViewControllerDelegate {
    
    weak var pageViewToContainerProtocol: PageViewToContainerProtocol?
    
    var currentViewController: UIViewController? {
        return viewControllers?.first
    }
    var index : Int? {
        guard currentViewController != nil else {
            return NSNotFound
        }
        return modelController.indexOfViewController(currentViewController!)
    }
    
    var scrollView: UIScrollView! {
        if _scrollView == nil {
            for view in self.view.subviews {
                if(view is UIScrollView) {
                    _scrollView = (view as! UIScrollView)
                }
            }
        }
        
        return _scrollView
    }
    
    var startViewControllerIndex : Int = 0
    private var _scrollView: UIScrollView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modelController.setupViewControllers()
        // Do any additional setup after loading the view, typically from a nib.
        // Configure the page view controller and add it as a child view controller.
        delegate = self
        dataSource = modelController
        if let startingViewController: UIViewController = self.modelController.viewControllerAtIndex(startViewControllerIndex) {
            let viewControllers = [startingViewController]
            setViewControllers(viewControllers, direction: .forward, animated: false, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var modelController = BaseModelController()
    
    // MARK: - UIPageViewController delegate methods
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
            pageViewToContainerProtocol?.pressTabButton()
    }
    
    func next() {
        guard  index != nil else { return  }
        jump(toIndex: index! + 1)
    }
    
    func previous() {
        guard  index != nil else { return  }
        jump(toIndex: index! - 1)
    }
    
    func jump(toIndex: Int, animated: Bool = true) {
        guard toIndex != NSNotFound && toIndex >= 0 else {
            return
        }
        guard  index != nil else { return  }

        let direction : UIPageViewControllerNavigationDirection = toIndex > index! ? .forward : .reverse
        if let viewController = modelController.viewControllerAtIndex(toIndex) {
            self.setViewControllers([viewController], direction: direction , animated: true, completion: {done in })
        }
    }
    
}
