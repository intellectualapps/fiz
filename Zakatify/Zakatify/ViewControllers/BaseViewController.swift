//
//  BaseViewController.swift
//  Zakatify
//
//  Created by MTQ on 10/24/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

}
