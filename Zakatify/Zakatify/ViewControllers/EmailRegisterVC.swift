//
//  EmailRegisterVC.swift
//  Zakatify
//
//  Created by Thang Truong on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EmailRegisterVC: UITableViewController, RegisterView {
    var disposeBag = DisposeBag()
    static var identifier = "EmailRegisterVC"
    @IBOutlet weak var tf_username: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var tf_repeatPassword: UITextField!
    
    
    var presenter: RegisterViewPresenter!


    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = RegisterPresenter(view: self)
        
        tf_username.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] in
            if let text = self.tf_username.text {
                if text.characters.count > 3 {
                    self.presenter.verify(username: text)
                }
            }
        }.addDisposableTo(disposeBag)
    }
    
    func doRegister() {
        guard let username = tf_username.text,
            let email = tf_email.text,
            let password = tf_password.text,
            let repeatPassowrd = tf_repeatPassword.text else {
            return
        }
        presenter.register(username: username, email: email, password: password, repeatPassword: repeatPassowrd)
    }

    // TODO : assign a.Thang
    private func gotoIntroView() {
        NotificationCenter.default.post(name: NotificationKey.gotoIntroView, object: nil)
    }
    
    func registerSuccess(user:UserInfo) {
        gotoIntroView()
    }
    
    // TODO : assign a.Thang
    func showLoadingVerify() {
        self.showLoading()
    }
    
    // TODO : assign a.Thang
    func hideLoadingVerify() {
        self.hideLoading()
    }
    
    func verify(username:String, status:Bool) {
        // kết quả trả về sau khi kiểm tra user nhập vào có còn avaiable không
        if username == tf_username.text, status == false {
            self.showAlert("\(username) is already in use")
        }
    }
    
}

