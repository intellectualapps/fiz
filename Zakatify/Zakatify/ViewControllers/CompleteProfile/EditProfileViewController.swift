//
//  EditProfileViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/4/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import IQKeyboardManagerSwift


class EditProfileViewController: UITableViewController {
    var disposeBag = DisposeBag()
    var percentCompleted: Double = 0
    
    @IBOutlet weak var iv_RateChart: CircleView!
    @IBOutlet weak var lb_rateValue: UILabel!
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var bt_camera: UIButton!
    //
    @IBOutlet weak var lb_fullName: UILabel!
    @IBOutlet weak var lb_username: UILabel!
    @IBOutlet weak var lb_portfolio: UILabel!
    @IBOutlet weak var lb_donations: UILabel!
    @IBOutlet weak var lb_followers: UILabel!
    @IBOutlet weak var lb_badges: UILabel!
    
    @IBOutlet weak var tf_fisrtname: UITextField!
    @IBOutlet weak var tf_lastname: UITextField!
    @IBOutlet weak var tf_mobile: UITextField!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_location: UITextField!
    @IBOutlet weak var tf_facebookEmail: UITextField!
    @IBOutlet weak var bt_connectFacebook: Button!
    @IBOutlet weak var tf_twitterEmail: UITextField!
    @IBOutlet weak var bt_connectTwitter: Button!
    @IBOutlet weak var verifyImageView: UIImageView!
    
    // Category
    @IBOutlet weak var uv_Categories: TagListView! {
        didSet {
            uv_Categories.alignment = .center
            uv_Categories.delegate = self
        }
    }

    
    var presenter: UserInfoViewPresenter?
    var categoryPresenter: CategoryPresenter?

    weak var container: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        addBackButtonDefault()
        self.addSwipeRight()
        tf_mobile.keyboardType = .phonePad
        tf_fisrtname.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userFirstName = self.tf_fisrtname.text ?? ""
            }.addDisposableTo(disposeBag)
        
        tf_lastname.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userLastName = self.tf_lastname.text ?? ""
            }.addDisposableTo(disposeBag)
        
        tf_mobile.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userMobile = self.tf_mobile.text ?? ""
            }.addDisposableTo(disposeBag)
        
        tf_location.rx.controlEvent(UIControlEvents.editingDidEnd).bind { [unowned self] () in
            self.presenter?.userLocation = self.tf_location.text ?? ""
            }.addDisposableTo(disposeBag)
        
        if let user = UserManager.shared.currentUser {
            presenter = UserInfoPresenter(view: self, model: user)
            categoryPresenter = CategoryPresenter(view: self)
            fillData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        categoryPresenter?.getAllCategory(completion: {[weak self] in
            self?.tableView?.reloadData()
        })
    }
}

// MARK: - CategoryView
extension EditProfileViewController: CategoryView {
    func added() {
        
    }
    
    func categoryFillData() {
        guard let presenter = categoryPresenter,
            let uv_Categories = uv_Categories else {
                return
        }
        uv_Categories.removeAllTags()
        let tags = presenter.categories.map { (category) in
            return category.description
        }
        uv_Categories.addTags(tags)
        let tagsView = uv_Categories.allTagsView()
        let selectedTags = presenter.userCategories.map { (category) in
            return category.description
        }
        let tagsViewNeedSelect = tagsView.filter { (tagview) -> Bool in
            if let title = tagview.currentTitle {
                return selectedTags.contains(title)
            }
            return false
        }
        
        DispatchQueue.main.async {
            for tagView in tagsViewNeedSelect {
                tagView.isSelected = true
            }
        }
    }
}

extension EditProfileViewController: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        tagView.isSelected = !tagView.isSelected
    }
    
    override func showLoading() {
        if let container = container {
            container.showLoading()
            return
        } else {
            super.showLoading()
        }
    }
    
    override func hideLoading() {
        if let container = container {
            container.hideLoading()
            return
        } else {
            super.hideLoading()
        }
    }
}

// MARK: - UserInfoView
extension EditProfileViewController: UserInfoView {
    func fillData() {
        guard let presenter = self.presenter else {
            return
        }
        iv_RateChart.value = CGFloat(percentCompleted)
        let percentCompletedToInt = Int(percentCompleted * 100)
        lb_rateValue.text = "\(CGFloat(percentCompletedToInt) / 100)%"
        //
        lb_fullName.attributedText = presenter.user.nameAttributedString
        lb_username.text = "@" + presenter.username
        //
        if let editAvatar = presenter.editPhoto {
            iv_avatar.image = editAvatar
        } else {
            if let url = presenter.photoUrl {
                iv_avatar.sd_setImage(with: url)
            }
        }
        
        tf_fisrtname.text = presenter.userFirstName
        tf_lastname.text = presenter.userLastName
        tf_email.text = presenter.userEmail
        tf_mobile.text = presenter.userMobile
        tf_location.text = presenter.userLocation
        
        tf_facebookEmail.text = presenter.userFacebookEmail
        bt_connectFacebook.setTitle(presenter.userFacebookEmail.isEmpty ? "Connect":"Disconnect", for: UIControlState.normal)
        
        
        tf_twitterEmail.text = presenter.userTwitterEmail
        bt_connectTwitter.setTitle(presenter.userTwitterEmail.isEmpty ? "Connect":"Disconnect", for: UIControlState.normal)
        
        lb_portfolio.text = String(presenter.user.portfolios)
        lb_followers.text = String(presenter.user.followers)
        lb_donations.text = String(presenter.user.donations)
        lb_badges.text = "None"
    }
    
    func saveSuccess() {
        let selectedTags = uv_Categories.selectedTags()
        let tagDescriptions = selectedTags.map { (tagview) in
            return tagview.currentTitle ?? ""
        }
        categoryPresenter?.add(categoryDescriptions: tagDescriptions)
    }
    
    @IBAction func choseImage(_ sender: Any) {
        choseImageSource()
    }
    
    private func choseImageSource() {

        UIApplication.shared.keyWindow?.endEditing(true)
        let alert = UIAlertController(title: "", message: "Chose image from", preferredStyle: UIAlertControllerStyle.actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let camera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (action) in
                self.choseImageFromCamera()
            })
            alert.addAction(camera)
        }
        
        let library = UIAlertAction(title: "Photo library", style: UIAlertActionStyle.default, handler: { (action) in
            self.choseImageFromPhotoLibrary()
        })
        alert.addAction(library)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
            
        }
        alert.addAction(cancel)
        self.present(alert, animated: true) {

        }
    }
    
    private func choseImageFromCamera() {
        UIImagePickerController.rx.createWithParent(self) { picker in
            picker.sourceType = .camera
            picker.allowsEditing = true
            }
            .flatMap { $0.rx.didFinishPickingMediaWithInfo }
            .take(1)
            .map { info -> UIImage? in
                if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                    return image
                } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    return image
                }
                return info[UIImagePickerControllerOriginalImage] as? UIImage
            }
            .subscribe { [unowned self] (event) in
                switch event {
                case .next(let image) :
                    self.presenter?.editPhoto = image
                    break
                case .error(let error):
                    self.showAlert(error.localizedDescription)
                    break
                case .completed :
                    break
                }
            }.addDisposableTo(disposeBag)
    }
    
    private func choseImageFromPhotoLibrary() {

        UIImagePickerController.rx.createWithParent(self) { picker in
            picker.sourceType = .photoLibrary
            picker.allowsEditing = true
            }
            .flatMap { $0.rx.didFinishPickingMediaWithInfo }
            .take(1)
            .map { info in
                return info[UIImagePickerControllerOriginalImage] as? UIImage
            }
            .subscribe { [unowned self] (event) in
                switch event {
                case .next(let image) :
                    self.presenter?.editPhoto = image
                    break
                case .error(let error):
                    self.showAlert(error.localizedDescription)
                    break
                case .completed :
                    break
                }
            }.addDisposableTo(disposeBag)
    }
    
    
    @IBAction func connectTwitter(_ sender: Any) {
        if presenter?.userTwitterEmail.isEmpty == false {
            presenter?.userTwitterEmail = ""
            fillData()
            return
        }
        presenter?.loginTwitter()
    }
    
    @IBAction func connectFacebook(_ sender: Any) {
        if presenter?.userFacebookEmail.isEmpty == false {
            presenter?.userFacebookEmail = ""
            fillData()
            return
        }
        presenter?.loginFacebook()
    }
    
    @IBAction func next(_ sender: Any) {
        self.view.endEditing(true)
        presenter?.saveChange()
    }
}

// MARK: - UITableViewDelegate
extension EditProfileViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 161
            } else if indexPath.row == 1 {
                return 55
            } else if indexPath.row == 2 {
                return 70
            }
        } else if indexPath.section == 1 {
            return 50
        } else if indexPath.section == 2 {
            guard let presenter = categoryPresenter else {
                return 49
            }
            let countCategories = CGFloat(presenter.categories.count)
            /// Giả sử:
            /// - Mỗi dòng có 3 tagView
            /// - Mỗi tagView có chiều cao = 36,
            /// - Khoảng cách bottom của tagView trên với top của tagView dưới là 8
            /// - listTagViews.top = UITableViewCell.top + 49
            let countLineTagView = Int(countCategories / 3) + 1
            let countLineTagViewToCGFloat = CGFloat(countLineTagView)
            
            return 49.0 + countLineTagViewToCGFloat * 36.0 + (countLineTagViewToCGFloat-1) * 8
        }
        return 0
    }
}
