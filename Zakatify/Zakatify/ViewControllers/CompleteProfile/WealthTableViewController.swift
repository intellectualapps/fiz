//
//  WealthTableViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class WealthTableViewController: UITableViewController {
    
    
    var cellData: [CalculateGoalViewController.CellData] = [.cash, .gold, .real, .inves]
    
    var zakat: Zakat?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.bounces = false
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        self.tableView.estimatedRowHeight = 70
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "NumberInputTableViewCell", bundle: nil) , forCellReuseIdentifier: NumberInputTableViewCell.identifier)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cellData.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NumberInputTableViewCell.identifier, for: indexPath) as? NumberInputTableViewCell else {
            return UITableViewCell()
        }

        // Configure the cell...
        let data = cellData[indexPath.row]
        cell.zakat = zakat
        cell.data = data
        return cell
    }
}
