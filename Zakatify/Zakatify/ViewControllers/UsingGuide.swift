//
//  ViewInCollectionView.swift
//  Layout
//
//  Created by Trương Thắng on 5/27/17.
//  Copyright © 2017 Trương Thắng. All rights reserved.
//
import UIKit
import Foundation

class UsingGuide {
    var titleLabel: String?
    var imageIntro: UIImage?
    var descriptionLabel: String?
    
    init(titleLabel: String, imageIntro: UIImage, descriptionLabel: String) {
        self.titleLabel = titleLabel
        self.imageIntro = imageIntro
        self.descriptionLabel = descriptionLabel
    }
}
