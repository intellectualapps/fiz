//
//  ContainerViewController.swift
//  Zakatify
//
//  Created by Thang Truong on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    @IBOutlet weak var alertView: UIVisualEffectView!
    @IBOutlet weak var topAlertViewConstraint: NSLayoutConstraint!
    
    struct Demension {
        static let constraintToOpen : CGFloat = 0
        static let barHeight : CGFloat = 20
        static let constraintToClose : CGFloat = -94 - Demension.barHeight
    }
    
    var constraintToClose : CGFloat?

    var isAlertShow = true {
        didSet {
            guard isAlertShow != oldValue else {return}
           configStage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isAlertShow = false
        setRadiusCorner()
    }
    
    func setRadiusCorner() {
        alertView.layer.cornerRadius = 10
        alertView.clipsToBounds = true
    }

    func configStage() {
        UIView.animate(withDuration: 0.35) { 
            self.topAlertViewConstraint.constant = self.isAlertShow ? 0 : -114
            self.view.layoutIfNeeded()

        }
    }
    
    @IBAction func toggleAlert() {
        isAlertShow = !isAlertShow
    }
    
}
