//
//  InviteTableViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/13/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import Contacts

class InviteTableViewController: UITableViewController, InvitePresenterView {

    var presenter: InvitePresenter?
    var refPresenter: TableViewPresenter? {
        guard let presenter = self.presenter else {
            return nil
        }
        return presenter
    }
    
    var userContactEmails: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSwipeRight()
        if let _ = self.presentingViewController {
            self.addLeftCloseButton()
        } else {
            self.addBackButtonDefault()
        }
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        presenter = InvitePresenter(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.refresh()
    }
    
    func refreshControlChange() {
        presenter?.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: InviteTableViewCell.reuseIdentifier, for: indexPath) as? InviteTableViewCell else {
            return UITableViewCell()
        }

        if let model = presenter?.invitation(index: indexPath.row) {
            cell.invitation = model
            cell.clickInviteBlock = { [unowned self] in
                self.presenter?.invite(email: model.invitee)
            }
        } else {
            cell.invitation = nil
            cell.clickInviteBlock = nil
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == lastElement - 1 {
            presenter?.loadmore()
        }
    }
}


class InviteTableViewCell: UITableViewCell {
    static let reuseIdentifier: String = "InviteTableViewCell"
    
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var bt_invite: Button!
    
    var clickInviteBlock:(()->Void)?
    
    var invitation: Invitation? {
        didSet {
            guard let invitation = invitation else {
                iv_avatar.image = nil
                lb_name.text = nil
                bt_invite.setTitle("", for: UIControlState.normal)
                return
            }
            lb_name.text = invitation.invitee
            bt_invite.setTitleColor(UIColor.buttonBackgroundColor, for: UIControlState.normal)
            bt_invite.isUserInteractionEnabled = true
            var status = "Invite"
            if invitation.state != .other {
                status = "Invited"
                bt_invite.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                bt_invite.isUserInteractionEnabled = false
            }
            bt_invite.setTitle(status, for: UIControlState.normal)
        }
    }
    @IBAction func click(_ sender: Any) {
        clickInviteBlock?()
    }
}
