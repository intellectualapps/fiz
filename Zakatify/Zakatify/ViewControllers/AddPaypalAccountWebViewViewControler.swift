//
//  AddPaypalAccountWebViewViewControler.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import PureLayout

typealias AddPaypalAccountCompletion = (Bool) -> ()
class AddPaypalAccountWebViewViewControler: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    fileprivate var bridge: SwiftWebViewBridge!
    var completion: AddPaypalAccountCompletion?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        self.addBackButtonDefault()
        self.addSwipeRight()
        self.bridge = SwiftWebViewBridge.bridge(webView, defaultHandler: { data, responseCallback in
            print("Swift received message from JS: \(data)")
        })

        //  SwiftJavaScriptBridge.logging = false

        self.bridge.registerHandlerForJS(handlerName: "addAccountSuccess", handler: { [weak self] jsonData, responseCallback in
            self?.completion?(true)
            self?.back()
        })

        self.bridge.registerHandlerForJS(handlerName: "addAccountFailed", handler: { [weak self] jsonData, responseCallback in
            self?.completion?(false)
            self?.back()
        })
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadWebContent()
    }
    
    func loadWebContent() {
        let url = Defaults.addPaypalUrl.getStringOrEmpty()
        if url.lenght > 0 {
            guard let newurl = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
                return
            }
            if let aurl = URL(string: newurl) {
                let urlRequest = URLRequest(url: aurl)
                self.webView?.loadRequest(urlRequest)
            }
        }
    }
}
