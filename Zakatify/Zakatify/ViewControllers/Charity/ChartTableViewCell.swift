//
//  ChartTableViewCell.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/25/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import Charts

class ChartTableViewCell: UITableViewCell {
    static let reuseIdentifier: String = "ChartTableViewCell"
    @IBOutlet weak var uv_chartContent: LineChartView! {
        didSet {
            guard let chart = uv_chartContent else {
                return
            }
            chart.legend.form = .none
            chart.chartDescription?.enabled = false
            chart.chartDescription?.text = ""
            chart.rightAxis.enabled = false
        }
    }
    var trendDatas: [[String:Any]] = [] {
        didSet {
            let months: [String] = trendDatas.map { json in
                guard let label = json["label"] as? String else {
                    return ""
                }
                return label
            }
            
            let values: [Int] = trendDatas.map { json in
                guard let value = json["value"] as? Int else {
                    return 0
                }
                return value
            }
            var chartEntry: [ChartDataEntry] = []
            for i in 0..<values.count {
                let entry = ChartDataEntry(x: Double(i), y: Double(values[i]))
                chartEntry.append(entry)
            }
            
            lineChartEntry = chartEntry
            
            guard let chart = uv_chartContent else {
                return
            }
            chart.xAxis.labelPosition = .bottom
            chart.xAxis.drawGridLinesEnabled = false
            chart.xAxis.spaceMin = 0.1
            chart.xAxis.spaceMax = 0.1
            chart.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
            chart.xAxis.granularity = 1
        }
    }
    
    var lineChartEntry: [ChartDataEntry] = [ChartDataEntry]() {
        didSet {
            let line1 = LineChartDataSet(values: lineChartEntry, label: "")
            line1.mode = .horizontalBezier
            line1.colors = [UIColor.buttonBackgroundColor]
            line1.circleColors = [UIColor.white]
            line1.circleHoleColor = UIColor.buttonBackgroundColor
            line1.circleRadius = 5
            line1.circleHoleRadius = 4
            
            let data = LineChartData()
            data.addDataSet(line1)
            guard let chtChart = self.uv_chartContent else { return }
            chtChart.data = data
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
