//
//  ReviewTableViewCell.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 9/25/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftDate

class ReviewTableViewCell: UITableViewCell {
    static let reuseIdentifier = "ReviewTableViewCell"
    @IBOutlet weak var iv_avatar: ImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_comment: UILabel!
    @IBOutlet weak var lb_date: UILabel!

    var review: Review? {
        didSet {
            guard let review = self.review else {
                return
            }
            if review.charityName.isEmpty {
                lb_name?.text = review.firstName + " " + review.lastName
            } else {
                lb_name?.text = review.charityName
            }
            lb_comment?.text = review.comment
            if let url = URL(string:review.photoUrl) {
                iv_avatar.sd_setImage(with: url,
                                      placeholderImage: UIImage(named: "noAvatar"))
                
            }

            if let creatDate = review.createDate {
                lb_date.text = creatDate.timeAgo()
            }
        }
    }
}
