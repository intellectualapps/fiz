//
//  CharityDetailsViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/6/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import Charts
import RxSwift
import SVPullToRefresh

class CharityDetailsTableViewController: UITableViewController, TableView {
    var refPresenter: TableViewPresenter?
    var disposed = DisposeBag()
    
    var presenter: CharityDetailViewPresenter? {
        didSet {
            presenter?.refresh()
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNotification()
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension

        /* Tạm ẩn đi chart review
        tableView.register(UINib(nibName: "ChartTableViewCell", bundle: nil), forCellReuseIdentifier: ChartTableViewCell.reuseIdentifier)
        */
        tableView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: ReviewTableViewCell.reuseIdentifier)

        presenter?.reviews
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (data) in
                if let strongself = self , data.count > 0 {
                    strongself.tableView.reloadData()
                }
            }).disposed(by: disposed)
        
    }

    private func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NotificationKey.review, object: nil)
    }
    
    func refresh() {
        presenter?.isRefresh = false
        presenter?.refresh()
    }
    
    @IBAction func add(_ sender: Any) {
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
        /* Tạm ẩn đi chart review
        return 3
        */
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 1
        }
        /* Tạm ẩn đi chart review
        else if (section == 1) {
            guard let presenter = self.presenter else { return 0 }
            return presenter.charity.trendDatas.count > 0 ? 1:0
        }
        */
        else {
            guard let presenter = self.presenter else { return 0 }
            return presenter.reviews.value.count
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        /* Tạm ẩn đi chart review
        else if (section == 1) {
            guard let presenter = self.presenter else { return 0 }
            return presenter.charity.trendDatas.count > 0 ? 30:0
        }
        */
        else {
            return 30
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = UIView.loadFromNibNamed(nibNamed: "TableViewSectionHeader") as? TableViewSectionHeader else {
            return nil
        }
        
        if section == 0 {
            return nil
        }
        /* Tạm ẩn đi chart review
        else if (section == 1) {
            view.lb_title.text = "RECENT DONATION ACTIVITY"
        }
        */
        else {
            if presenter?.reviews.value.count == 0 {
                view.lb_title.text = "NO REVIEWS"
            } else {
                view.lb_title.text = "LATEST REVIEWS"
            }
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath) as? CharityHeaderTableViewCell else {
                return UITableViewCell()
            }
            
            // Configure the cell...
            cell.delegate = self
            cell.presenter = presenter
            
            return cell
        }
        /* Tạm ẩn đi chart review
        else if (indexPath.section == 1) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ChartTableViewCell.reuseIdentifier, for: indexPath) as? ChartTableViewCell else {
                return UITableViewCell()
            }
            if let presenter = self.presenter {
                cell.trendDatas = presenter.charity.trendDatas
            }
            return cell
        }
        */
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ReviewTableViewCell.reuseIdentifier, for: indexPath) as? ReviewTableViewCell else {
                return UITableViewCell()
            }
            if let review = presenter?.reviews.value[indexPath.row] {
                cell.review = review
            }
            return cell
        }
    }

}

extension CharityDetailsTableViewController: CharityHeaderTableViewCellDelegate {
    func clickWebsite() {
        guard let presenter = presenter else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(presenter.charityWebsite!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(presenter.charityWebsite!)
        }
    }
    
    func clickEmail() {
        guard let presenter = presenter else { return }
        presentEmailVC(recipients: [presenter.charityEmail])
    }
    
    func clickDonate() {
        guard let token = UserManager.shared.authenToken,
            let username = UserManager.shared.currentUser?.username,
            let charityID = presenter?.charity.id else {
            return
        }

        let urlString = "https://zakatify.com/manual-donation/?t=\(token)&username=\(username)&charity-id=\(charityID)"
        guard let url = URL(string: urlString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func clickAddPortfolio() {
        if presenter?.userPortfolio == true {
            presenter?.removeFromPortfolio()
        } else {
            presenter?.addToPortfolio()
        }
    }
}
