//
//  PaymentViewModel.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 4/29/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class PaymentViewModel: NSObject {
    let dispose = DisposeBag()
    
    var paypalAccounts = Variable([PaypalAccount]())
    
    func getPaypalsAccount(completion: ((_ accounts: [PaypalAccount]) -> ())? = nil) {
        GetPaypalAccountsService.getPaypalAccount {[weak self] (response, error) in
            guard let `self` = self else {
                return
            }
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
                completion?([])
            } else if let accounts = response as? [PaypalAccount] {
                self.paypalAccounts.value = accounts
                completion?(accounts)
            }
        }
    }
    
    func deleteOption(payment: Payment, username: String, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        let encoding = Encoding.forMethod(method: .delete)
        let path = Path.paypalDelete(payment.id).path
        let service = PaymentDeleteService(apiPath: path, method: .delete, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
    
    func setPrimary(payment: Payment, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        let encoding = Encoding.forMethod(method: .put)
        let path = Path.paypalSetPrimary(payment.id).path
        let service = PaymentSetPrimaryService(apiPath: path, method: .put, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
    
    func addPaymenOption(username: String,primary: Int, type: PaymentType, digit: Int, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(type.rawValue, forKey: "payment-type")
        parmas.setValue(digit, forKey: "last-four")
        parmas.setValue(primary, forKey: "is-primary")
        parmas.setValue(UserManager.shared.currentUser?.email, forKey: "email")
        parmas.setValue(UserManager.shared.authenToken, forKey: "token")
        let encoding = Encoding.forMethod(method: .put)
        let path = Path.paypalSetPrimary(username).path
        let service = PaymentAddService(apiPath: path, method: .get, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
}
