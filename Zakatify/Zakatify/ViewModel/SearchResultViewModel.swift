//
//  SearchResultsViewModel.swift
//  Zakatify
//
//  Created by MTQ on 7/27/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class SearchResultViewModel {
    var resultSearchZakatifiers: [UserPublicProfile] = []
    var zakatifierPage = 1
    
    func zakatifierSearch(username: String, keyword: String, page: Int, size: Int, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(username, forKey: "username")
        parmas.setValue(keyword, forKey: "query")
        parmas.setValue(page, forKey: "page-number")
        parmas.setValue(size, forKey: "page-size")
        let encoding = Encoding.forMethod(method: .get)
        let path = Path.zakatifierSearch.path
        let service = ZakatifierSearchServices(apiPath: path, method: .get, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
    
    public func validateZakatifierModel(zakatifierModel: UserPublicProfile) -> Bool {
        for userPublicProfile in resultSearchZakatifiers {
            if userPublicProfile.user.creationDate == zakatifierModel.user.creationDate &&
                userPublicProfile.user.hash == zakatifierModel.user.hash {
                return false
            }
        }
        return true
    }
}
