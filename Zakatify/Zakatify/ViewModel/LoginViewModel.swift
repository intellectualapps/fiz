//
//  LoginViewModel.swift
//  Zakatify
//
//  Created by MTQ on 8/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class LoginViewModel {
    func loginFacebook(email: String, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(email, forKey: "id")
        parmas.setValue("", forKey: "password")
        parmas.setValue(UserAPI.AuthenType.facebook.rawValue, forKey: "auth-type")
        let encoding = Encoding.forMethod(method: .post)
        let path = Path.loginFacebook.path
        
        let service = LoginFacebookService(apiPath: path, method: .post, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
}
