//
//  CharityViewModel.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/10/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class CharityViewModel: NSObject {
    let dispose = DisposeBag()
    
    func getCharitiesReview(id: Int, page: Int, size: Int, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(page, forKey: "page-number")
        parmas.setValue(size, forKey: "page-size")
        let encoding = Encoding.forMethod(method: .get)
        let path = Path.chartityReview(id).path
        let service = CharityReviewService(apiPath: path, method: .get, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
    
    func getCharityDetail(id: Int, username: String, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(username, forKey: "username")
        let encoding = Encoding.forMethod(method: .get)
        let path = Path.charityDetail(id).path
        let service = CharityDetailService(apiPath: path, method: .get, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
    
    func getCharitiesListl(username: String, page: Int, size: Int, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(page , forKey: "page-number")
        parmas.setValue(size, forKey: "page-size")
        let encoding = Encoding.forMethod(method: .get)
        let path = Path.charityList(username).path
        let service = CharitiesListService(apiPath: path, method: .get, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        service.doExecute(completion)
    }
    
    var charitySearchService: CharitySearchService?
    
    func charitySearch(username: String, keyword: String, page: Int, size: Int, completion: @escaping NetworkServiceCompletion) {
        let parmas = RequestParams()
        parmas.setValue(username, forKey: "username")
        parmas.setValue(keyword, forKey: "query")
        parmas.setValue(page, forKey: "page-number")
        parmas.setValue(size, forKey: "page-size")
        let encoding = Encoding.forMethod(method: .get)
        let path = Path.charitySearch.path
        charitySearchService?.cancel()
        charitySearchService = CharitySearchService(apiPath: path, method: .get, requestParam: parmas, paramEncoding: encoding, retryCount: 1)
        
        charitySearchService?.doExecute(completion)
    }
}
