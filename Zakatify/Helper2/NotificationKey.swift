//
//  NotificationKey.swift
//  Zakatify
//
//  Created by Thang Truong on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

struct NotificationKey {
    static let showForgotPasswordVC = Notification.Name.init("showForgotPasswordVC")
    static let doLogin = Notification.Name.init("doLogin")
    static let doRegister = Notification.Name.init("doRegister")
    static let gotoIntroView = Notification.Name.init("gotoIntroView")
    static let gotoHome = Notification.Name.init("gotoHome")
    static let review = Notification.Name("review")
}
