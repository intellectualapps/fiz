//
//  DonationProgess.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/12/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

protocol DonationProgess {
    var points: Float {get set}
    var goal: Float {get set}
    var totalDonation: Double {get set}
    var percentCompleted: Double {get set}
}

class UserDonationProgess: DonationProgess, Mappable {
    enum JSONKey: String {
        case points
        case goal = "zakatGoal"
        case totalDonation = "totalDonation"
        case percentageCompleted = "percentageCompleted"
    }
    var points: Float = 0
    var goal: Float = 0
    var totalDonation: Double = 0
    var percentCompleted: Double = 0
    
    init() {
        
    }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        points <- map[JSONKey.points.rawValue]
        goal <- map[JSONKey.goal.rawValue]
        totalDonation <- map[JSONKey.totalDonation.rawValue]
        percentCompleted <- map[JSONKey.percentageCompleted.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        return [JSONKey.points.rawValue:points,
                JSONKey.goal.rawValue:goal,
                JSONKey.totalDonation.rawValue:totalDonation,
                JSONKey.percentageCompleted.rawValue:percentCompleted]
    }
}
