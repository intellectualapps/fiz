
//
//  SerialInfo.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

enum SerialStatus: String {
    case inStock = "in_stock"
    case sold = "sold"
}

class SerialInfo: NSObject {
    var status = ""
    var binNumber = ""
    var receiveDate: Date?
    var qtyInStock = ""
    var itemNumber = ""
    var desc = ""
    var invoice = ""
    var customerId = ""
    var caseNumber = ""

    override init() {
        super.init()
    }

    /*
     Created by: dungnv
     Created date: 2/5/17
     Purpose:
     Get Serial instance from dictionary response from server in api Lookup serial
     {
     "STATUS": "IN_STOCK",
     "BIN_NUMBER": "3-12-A1",
     "RECEIVE_DATE": "2017-03-12",
     "QTY_IN_STOCK": "102",
     "ITEM_NUMBER": "2983749",
     "DESCRIPTION": "ITEM DESCRIPTION 2983749"
     }
     */
    convenience init(dict: [String: Any]) {
        self.init()
        self.status = dict.stringOrEmptyForKey(key: "STATUS")
        self.binNumber = dict.stringOrEmptyForKey(key: "BIN_NUMBER")
        let dateStr = dict.stringOrEmptyForKey(key: "RECEIVE_DATE")
        self.receiveDate = Date.date(fromString: dateStr, format: "yyyy-MM-dd")
        self.qtyInStock = dict.stringOrEmptyForKey(key: "QTY_IN_STOCK")
        self.itemNumber = dict.stringOrEmptyForKey(key: "ITEM_NUMBER")
        self.desc = dict.stringOrEmptyForKey(key: "DESCRIPTION")
        self.caseNumber = dict.stringOrEmptyForKey(key: "CASE_NUMBER")
    }

    /*
     Generate data source to display to tableview
     */
//    func generateDateSource(cellIdentifier: String) -> [SectionInfo] {
//        let section = SectionInfo(indentify: "", height: 0.0, index: nil, contentObj: nil, constructor: { () -> ([CellInfo]) in
//            var cells = [CellInfo]()
//            ///Feedback 12/7/17: Need show serial number cell
//            //serial number cell
//            var cell =  CellInfo(indentify: cellIdentifier,
//                                 aCellId: 0,
//                                 height: 50.0,
//                                 content: self.itemNumber,
//                                 title: "Serial Number",
//                                 cellColor: .white)
//            cells.append(cell)
//
//            //status cell
//            cell =  CellInfo(indentify: cellIdentifier,
//                                 aCellId: 0,
//                                 height: 50.0,
//                                 content: self.status,
//                                 title: "STATUS",
//                                 cellColor: .white)
//            cells.append(cell)
//
//            //Bin
//            // FB: 3/5/17: Do not need show bin number in case status = sold
//            if status.lowercased() != SerialStatus.sold.rawValue {
//                cell =  CellInfo(indentify: cellIdentifier,
//                                 aCellId: 0,
//                                 height: 50.0,
//                                 content: self.binNumber,
//                                 title: "Bin",
//                                 cellColor: .white)
//                cells.append(cell)
//            }
//
//            //receive date
//            var dateStr = ""
//            if let date = self.receiveDate {
//                dateStr = String.string(fromDate: date, format: "yyyy-MM-dd")
//            }
//            cell =  CellInfo(indentify: cellIdentifier,
//                             aCellId: 0,
//                             height: 50.0,
//                             content: dateStr,
//                             title: "Rcv Date",
//                             cellColor: .white)
//            cells.append(cell)
//
//            //in stock
//            cell =  CellInfo(indentify: cellIdentifier,
//                             aCellId: 0,
//                             height: 50.0,
//                             content: self.qtyInStock,
//                             title: "In Stock",
//                             cellColor: .white)
//            cells.append(cell)
//
//            //Item number
//            cell =  CellInfo(indentify: "SerialCell",
//                             aCellId: 0,
//                             height: 50.0,
//                             content: self.caseNumber,
//                             title: "Case #",
//                             cellColor: .white)
//            cells.append(cell)
//
//            //Bin QTy
//            cell =  CellInfo(indentify: cellIdentifier,
//                             aCellId: 0,
//                             height: 50.0,
//                             content: self.qtyInStock,
//                             title: "Bin Qty",
//                             cellColor: .white)
//            cells.append(cell)
//
//            //FB 3/5/17: do not need show invoice and custoemr # in case status = in stock
//            if status.lowercased() != SerialStatus.inStock.rawValue {
//                //Invoice
//                cell =  CellInfo(indentify: cellIdentifier,
//                                 aCellId: 0,
//                                 height: 50.0,
//                                 content: self.invoice,
//                                 title: "Invoice",
//                                 cellColor: .white)
//                cells.append(cell)
//
//                //Invoice
//                cell =  CellInfo(indentify: cellIdentifier,
//                                 aCellId: 0,
//                                 height: 50.0,
//                                 content: self.customerId,
//                                 title: "Customer #",
//                                 cellColor: .white)
//                cells.append(cell)
//            }
//            return cells
//        })
//        return [section]
//    }
}
