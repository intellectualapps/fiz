//
//  Board.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/12/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper
protocol Board {
    var rank: Int {get set}
    var points: Int {get set}
}

extension Board {
    var descriptionAttributedString: NSAttributedString {
        let attributedString = NSMutableAttributedString(string: "Points ")
        attributedString.append(NSAttributedString(string: "\(points)",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray,
                         NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]))
        attributedString.append(NSAttributedString(string: "   Rank "))
        attributedString.append(NSAttributedString(string: "\(rank)",
            attributes: [NSAttributedString.Key.foregroundColor:UIColor.darkGray,
                         NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]))
        return attributedString
    }
}

struct UserBoard: Board, Mappable {
    enum JSONKey: String {
        case rank
        case points
    }
    var rank: Int = 0
    var points: Int = 0
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        rank <- map[JSONKey.rank.rawValue]
        points <- map[JSONKey.points.rawValue]
    }
    
    func toJSON() -> [String : Any] {
        return [JSONKey.rank.rawValue:rank,
                JSONKey.points.rawValue:points]
    }
}
