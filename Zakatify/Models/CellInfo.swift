//
//  CellInfo.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

class CellInfo: NSObject {
    var content: Any?
    //This is xib file name. Used for table view dequeue
    var identifier = ""
    var height: CGFloat = 0
    var title: String = ""
    var cellId = 0
    var cellColor: UIColor = .white

    convenience init(indentify: String,
                     aCellId: Int = 0,
                     height: CGFloat,
                     content: Any? = nil,
                     title: String? = nil,
                     cellColor: UIColor = .white) {
        self.init()
        self.cellColor = cellColor
        self.identifier = indentify
        self.height = height
        self.content = content
        self.title = title ?? ""
        cellId = aCellId
    }
}
