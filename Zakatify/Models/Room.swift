//
//  Room.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 10/9/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper
import Firebase

class Room: Mappable {
    var dispose = DisposeBag()
    var unreadObserble = Variable(0)
    var partnersHandle: UInt = 0
    enum JSONKey: String {
        case id = "messageId"
        case user = "requestingParticipant"
        case otherParticipant = "otherParticipant"
    }
    var id: String = ""
    var user: UserInfo?
    var otherParticipant: ZakatifierInfo?
    var lastDate: Date?
    var lastMessage = ""
    var queryLimit = 100

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        id <- map[JSONKey.id.rawValue]
        user <- map[JSONKey.user.rawValue]
        otherParticipant <- map[JSONKey.otherParticipant.rawValue]
    }

    func stopListenUnread() {
        FirebaseConstants.refs.databaseConnections.removeObserver(withHandle: self.partnersHandle)
    }

    func startListenUnread() {
        self.stopListenUnread()
        //unread
        let child = FirebaseConstants.refs.databaseConnections.child(self.id).child("messages")
        self.partnersHandle = child.queryOrdered(byChild: "read").queryEqual(toValue: 0).observe(.value, with: {[weak self] (snapshot) in

            DispatchQueue.main.async {
                if let dict = snapshot.value as? NSDictionary {
                    var counter = 0
                    if let allkeys = dict.allKeys as? [String] {

                        for key in allkeys {
                            if let value = dict.value(forKey: key) as? [String: Any] {
                                let author = value.stringOrEmptyForKey(key: "author")
                                if UserManager.shared.currentUser?.username != author {
                                    counter += 1
                                }
                            }
                        }
                    }

                    self?.unreadObserble.value = counter
                }
            }
        })
    }
}
