//
//  Tag.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol Tag {
    var id: Int {get set}
    var description: String {get set}
}
