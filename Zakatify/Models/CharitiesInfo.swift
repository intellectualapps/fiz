//
//  CharitiesInfo.swift
//  Zakatify
//
//  Created by nguyen tuan on 6/9/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

class CharitiesInfo {
    var charities: [CharityDetail] = []
    var pageNumber = 0
    var pageSize = 0
    var totalCount = 0
    
    init(dict: [String: Any]) {
        if let _charities = dict["charities"] as? [[String: Any]] {
            for charity in _charities {
                if let _charity = Charity(JSON: charity) {
                    charities.append(_charity)
                }
            }
        }
        pageNumber = dict.intForKey(key: "pageNumber")
        pageSize = dict.intForKey(key: "pageSize")
        totalCount = dict.intForKey(key: "totalCount")
    }
}
