//
//  DateFormatter.swift
//  Zakatify
//
//  Created by MTQ on 8/14/18.
//  Copyright © 2018 Dht. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    enum Format: String {
        case iso8601 = "MMM dd, h:mm a"
        case date = "MMM d, yyyy"
        case japaneseDate = "yyyy年MM月dd日"
        case shortTime = "hh:mm a"
        case shortTime2 = "E, HH:mm a"
        
        var instance: DateFormatter {
            switch self {
            default:
                let dateFomatter111 = DateFormatter()
                dateFomatter111.dateFormat = self.rawValue
                dateFomatter111.amSymbol = "AM"
                dateFomatter111.pmSymbol = "PM"
                dateFomatter111.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                dateFomatter111.locale = Locale(identifier: "en_US_POSIX")
                return dateFomatter111
            }
        }
    }
}

extension Date {
    func toJapaneseDate() -> String {
        let formatter = DateFormatter.Format.japaneseDate.instance
        return formatter.string(from: self)
    }
    
    func toShortTime() -> String {
        let formatter = DateFormatter.Format.shortTime.instance
        return formatter.string(from: self)
    }
    
    func toShortTime2() -> String {
        let formatter = DateFormatter.Format.shortTime2.instance
        return formatter.string(from: self)
    }
    
    func toDate() -> String {
        let formatter = DateFormatter.Format.date.instance
        return formatter.string(from: self)
    }
    
    func toISO8601() -> String {
        let formatter = DateFormatter.Format.iso8601.instance
        return formatter.string(from: self)
    }
    
    var minimumDate: Date {
        var dateComponents = DateComponents()
        dateComponents.year = 1900
        dateComponents.month = 1
        dateComponents.day = 1
        let userCalendar = Calendar.current
        let minDate = userCalendar.date(from: dateComponents)
        return minDate ?? Date()
    }
    
    var timestamp: Int {
        return Int(Date().timeIntervalSince1970 * 1000)
    }
    
    func addedBy(minutes:Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
