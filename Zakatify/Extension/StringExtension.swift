//
//  StringExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func removeSpace() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func getDateUTCFromString(format: String = "MM dd, yyyy") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "en-US")
        return formatter.date(from: self)
    }
    
    var lenght: Int {
        get {
            return self.count
        }
    }

    /*
     Get string localized in Locazable file.
     */
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }

    /**
     * Calulate size for text. 
     * boundSize: This is max bound text can display in
     */
    func sizeWithBoundSize(boundSize: CGSize,
                           option: NSStringDrawingOptions,
                           lineBreakMode: NSLineBreakMode = NSLineBreakMode.byTruncatingTail,
                           font: UIFont) -> CGSize {
        if self.isEmpty {
            return CGSize.zero
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = lineBreakMode
        return (self as NSString).boundingRect(with: boundSize,
                                               options: option,
                                               attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle],
                                               context: nil).size
    }

    /**
     * Get date string from date with specify format (yyyy/MM/dd ....)
     */
    static func string(fromDate: Date, format: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format
        return dateFormater.string(from: fromDate)
    }

    /**
     * Generate udid string. This value will unique within current devive.
     * format like this: AAAA-BBBB-CCCC-DDDD....
     */
    static func newUdid() -> String {
        if let aid = CFUUIDCreate(nil) {
            return CFUUIDCreateString(nil, aid) as String
        }
        return ""
    }

    /**
     * Return path of document directory in app folder.
     */
    static func documentDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(
            .documentDirectory,
            .userDomainMask,
            true)
        return paths.first ?? ""
    }

    /**
     * Return the normal log file
     * This file will log the status of DataMan System such as : conntect, disconnect,...
     */
    static func normalLogFilePath() -> String {
        let filename = "normalLog.csv"
        return String.documentDirectoryPath() + "/" + filename
    }

    /**
     * Return Data log file path
     */
    static func dataLogFilePath() -> String {
        let filename = "dataLog.csv"
        return String.documentDirectoryPath() + "/" + filename
    }

    /**
     * Return error log file path
     */
    static func errorLogFilePath() -> String {
        let filename = "error.csv"
        return String.documentDirectoryPath() + "/" + filename
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
