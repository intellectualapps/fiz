//
//  UIStoryboardExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    class func main() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    class func signupSignin() -> UIStoryboard {
        return UIStoryboard(name: "SignupSigninStoryboard", bundle: nil)
    }
    
    class func home() -> UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    
    class func completeProfile() -> UIStoryboard {
        return UIStoryboard(name: "CompleteProfile", bundle: nil)
    }
    
    class func charity() -> UIStoryboard {
        return UIStoryboard(name: "Charity", bundle: nil)
    }
    class func zakatifier() -> UIStoryboard {
        return UIStoryboard(name: "Zakatifier", bundle: nil)
    }

    class func about() -> UIStoryboard {
        return UIStoryboard(name: "About", bundle: nil)
    }

    class func message() -> UIStoryboard {
        return  UIStoryboard(name: "Message", bundle: nil)
    }
    
    class func invite() -> UIStoryboard {
        return  UIStoryboard(name: "Invite", bundle: nil)
    }
    
    class func webview() -> UIStoryboard {
        return  UIStoryboard(name: "WebView", bundle: nil)
    }
}
