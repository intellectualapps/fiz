//
//  BaseImageView.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 3/14/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewProtocol.h"
#import "DhtHandle.h"
#import "Utils.h"
@interface BaseImageView : UIImageView<BaseViewProtocol, DhtWatcher>
//This is used to detect view in view storage
@property (nonatomic, strong) NSString * viewIdentifier;
@property (nonatomic, strong) DhtHandle *handler;
@property (nonatomic, assign) BOOL donotCacheImage;
@property (nonatomic, assign) BOOL needProgress;
/*
 This is used to detect state and decide update content or not
 example : when you want display image of url (x) you need check the current image of UIImageView is displayed that image or not.
 */
@property (nonatomic, strong) NSString * viewStateIdentifier;
@property (nonatomic, strong) NSString *currentUrl;
- (void) loadImage:(UIImage *)image;
- (UIImage *)currentImage;
- (void)loadImageAtPath:(NSString *)path placeholder:(UIImage *)placeholderImage;
- (void)reset;
- (void)cancel;
@end
