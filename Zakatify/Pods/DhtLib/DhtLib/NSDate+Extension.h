//
//  NSDate+Extension.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MonthDateRange : NSObject
@property (nonatomic, assign) NSInteger countOfWeaks;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@end


@interface DateRange: NSObject
@property (nonatomic, assign) NSInteger year;
@property (nonatomic, assign) NSInteger month;
@property (nonatomic, assign) NSInteger weekOfMonth;
@property (nonatomic, assign) NSInteger day;
@end

@interface NSDate(calendar)
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;
- (NSDate *)dateByAddingMonths:(NSInteger)month;
- (NSDate *)firstDayOfMonth;
- (NSDate *)dateAtEndOfMonth;
- (NSDate *)dateByAddingDays:(NSInteger)days;
- (NSDate *)dateByMinusDays:(NSInteger)days;
- (NSDate *)dateAtStartOfDay;
- (NSDate *)dateAtEndOfDay;
- (NSDate *)dateAtStartOfMonth;
- (NSDate *)dateByAddMonth:(NSInteger)month;
- (NSInteger)numberOfDayInMonth;
- (NSInteger)weakday;
- (NSInteger)year;
- (NSInteger)month;
- (NSInteger)day;

- (NSDateComponents *)componentsForDate;
- (MonthDateRange *)monthDateRange;
- (DateRange *)dateRange;

@end
