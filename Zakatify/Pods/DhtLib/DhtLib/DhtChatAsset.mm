//
//  DhtChatAsset.m
//  DhtChat
//
//  Created by Nguyen Van Dung on 10/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtChatAsset.h"
#import "DhtChatUtils.h"

static bool readCGFloat(NSString *string, int &position, CGFloat &result) {
    int start = position;
    bool seenDot = false;
    int length = (int)string.length;
    while (position < length) {
        unichar c = [string characterAtIndex:position];
        position++;

        if (c == '.') {
            if (seenDot) {
                return false;
            } else {
                seenDot = true;
            }
        } else if (c < '0' || c > '9') {
            if (position == start) {
                result = 0.0f;
                return true;
            } else {
                result = [[string substringWithRange:NSMakeRange(start, position - start)] floatValue];
                return true;
            }
        }
    }
    if (position == start) {
        result = 0.0f;
        return true;
    } else {
        result = [[string substringWithRange:NSMakeRange(start, position - start)] floatValue];
        return true;
    }
    return true;
}

void DrawSvgPath(CGContextRef context, NSString *path) {
    int position = 0;
    int length = (int)path.length;

    while (position < length) {
        unichar c = [path characterAtIndex:position];
        position++;

        if (c == ' ') {
            continue;
        }

        if (c == 'M') { // M
            CGFloat x = 0.0f;
            CGFloat y = 0.0f;
            readCGFloat(path, position, x);
            readCGFloat(path, position, y);
            CGContextMoveToPoint(context, x, y);
        } else if (c == 'L') { // L
            CGFloat x = 0.0f;
            CGFloat y = 0.0f;
            readCGFloat(path, position, x);
            readCGFloat(path, position, y);
            CGContextAddLineToPoint(context, x, y);
        } else if (c == 'C') { // C
            CGFloat x1 = 0.0f;
            CGFloat y1 = 0.0f;
            CGFloat x2 = 0.0f;
            CGFloat y2 = 0.0f;
            CGFloat x = 0.0f;
            CGFloat y = 0.0f;
            readCGFloat(path, position, x1);
            readCGFloat(path, position, y1);
            readCGFloat(path, position, x2);
            readCGFloat(path, position, y2);
            readCGFloat(path, position, x);
            readCGFloat(path, position, y);

            CGContextAddCurveToPoint(context, x1, y1, x2, y2, x, y);
        } else if (c == 'Z') { // Z
            CGContextClosePath(context);
            CGContextFillPath(context);
            CGContextBeginPath(context);
        }
    }
}

static UIImage *playImageWithColor(UIColor *color)
{
    CGFloat radius = 37.0f;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(radius, radius), false, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(0.0f, 0.0f, radius, radius));

    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);

    CGContextTranslateCTM(context, -0.5, 0.5);
    CGFloat factor = 28.0f / 34.0f;
    CGContextScaleCTM(context, 0.5f * factor, 0.5f * factor);

    DrawSvgPath(context, @"M39.4267651,27.0560591 C37.534215,25.920529 36,26.7818508 36,28.9948438 L36,59.0051562 C36,61.2114475 37.4877047,62.0081969 39.3251488,60.7832341 L62.6748512,45.2167659 C64.5112802,43.9924799 64.4710515,42.0826309 62.5732349,40.9439409 L39.4267651,27.0560591 Z");

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

static UIImage *pauseImageWithColor(UIColor *color)
{
    CGFloat radius = 37.0f;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(radius, radius), false, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(0.0f, 0.0f, radius, radius));

    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);

    CGFloat factor = 28.0f / 34.0f;
    CGContextTranslateCTM(context, 0.5, 0.5);
    CGContextScaleCTM(context, 0.5f * factor, 0.5f * factor);

    DrawSvgPath(context, @"M29,30.0017433 C29,28.896211 29.8874333,28 30.999615,28 L37.000385,28 C38.1047419,28 39,28.8892617 39,30.0017433 L39,57.9982567 C39,59.103789 38.1125667,60 37.000385,60 L30.999615,60 C29.8952581,60 29,59.1107383 29,57.9982567 L29,30.0017433 Z M49,30.0017433 C49,28.896211 49.8874333,28 50.999615,28 L57.000385,28 C58.1047419,28 59,28.8892617 59,30.0017433 L59,57.9982567 C59,59.103789 58.1125667,60 57.000385,60 L50.999615,60 C49.8952581,60 49,59.1107383 49,57.9982567 L49,30.0017433 Z");

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

static UIImage *downloadImageWithColor(UIColor *color, UIColor *backgroundColor)
{
    CGFloat radius = 37.0f;
    CGFloat diameter = radius;
    CGFloat lineWidth = 2.0f;
    CGFloat width = ceil(radius / 2.5f);
    CGFloat height = ceil(radius / 2.0f) - 1.0f;

    UIGraphicsBeginImageContextWithOptions(CGSizeMake(radius, radius), false, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(0.0f, 0.0f, radius, radius));

    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetLineCap(context, kCGLineCapRound);

    CGPoint mainLine[] = {
        CGPointMake((diameter - lineWidth) / 2.0f + lineWidth / 2.0f, (diameter - height) / 2.0f + lineWidth / 2.0f),
        CGPointMake((diameter - lineWidth) / 2.0f + lineWidth / 2.0f, (diameter + height) / 2.0f - lineWidth / 2.0f)
    };

    CGPoint arrowLine[] = {
        CGPointMake((diameter - lineWidth) / 2.0f + lineWidth / 2.0f - width / 2.0f, (diameter + height) / 2.0f + lineWidth / 2.0f - width / 2.0f),
        CGPointMake((diameter - lineWidth) / 2.0f + lineWidth / 2.0f, (diameter + height) / 2.0f + lineWidth / 2.0f),
        CGPointMake((diameter - lineWidth) / 2.0f + lineWidth / 2.0f, (diameter + height) / 2.0f + lineWidth / 2.0f),
        CGPointMake((diameter - lineWidth) / 2.0f + lineWidth / 2.0f + width / 2.0f, (diameter + height) / 2.0f + lineWidth / 2.0f - width / 2.0f),
    };

    CGContextStrokeLineSegments(context, mainLine, sizeof(mainLine) / sizeof(mainLine[0]));
    CGContextStrokeLineSegments(context, arrowLine, sizeof(arrowLine) / sizeof(arrowLine[0]));

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

static UIImage *cancelImageWithColor(UIColor *color, UIColor *backgroundColor)
{
    CGFloat radius = 37.0f;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(radius, radius), false, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGFloat diameter = radius;
    CGFloat lineWidth = 2.0f;
    CGFloat crossSize = 16.0f;

    CGContextSetBlendMode(context, kCGBlendModeCopy);

    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(0.0f, 0.0f, radius, radius));

    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, lineWidth);

    CGPoint crossLine[] = {
        CGPointMake((diameter - crossSize) / 2.0f, (diameter - crossSize) / 2.0f),
        CGPointMake((diameter + crossSize) / 2.0f, (diameter + crossSize) / 2.0f),
        CGPointMake((diameter + crossSize) / 2.0f, (diameter - crossSize) / 2.0f),
        CGPointMake((diameter - crossSize) / 2.0f, (diameter + crossSize) / 2.0f),
    };

    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextStrokeLineSegments(context, crossLine, sizeof(crossLine) / sizeof(crossLine[0]));

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

@implementation DhtChatAsset
+ (DhtChatAsset *) instance {
    static DhtChatAsset *singleton = nil;
    static dispatch_once_t oneToken ;
    dispatch_once(&oneToken, ^{
        singleton = [[DhtChatAsset alloc]init];
    });
    return singleton;
}

- (UIImage *)sentFailButtonImage {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [UIImage imageNamed:@"UnsentButton_small"];
    });
    return image;
}

- (UIImage *)incomingTemplateTextBackgroundImageSelected {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [[UIImage imageNamed:@"Incoming_template_selected.png"] stretchableImageWithLeftCapWidth: 16 topCapHeight: 16];

    });
    return image;
}

- (UIImage *)audioMessageBackgroundImage {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [[UIImage imageNamed:@"audio_background.png"] stretchableImageWithLeftCapWidth: 16 topCapHeight: 16];

    });
    return image;
}

- (UIImage *)incomingTemplateTextBackgroundImage {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [[UIImage imageNamed:@"Incoming_template.png"] stretchableImageWithLeftCapWidth: 16 topCapHeight: 16];

    });
    return image;
}

- (UIImage *)incomingTextBackgroundImage {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [[UIImage imageNamed:@"IncomingFull.png"] stretchableImageWithLeftCapWidth: 23 topCapHeight: 16];

    });
    return image;
}

- (UIImage *)outgoingTextBackgroundImage {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [[UIImage imageNamed:@"OutgoingFull.png"] stretchableImageWithLeftCapWidth: 17 topCapHeight:16];
    });
    return image;
}

- (UIImage *)defaultAvatarImage {
    static UIImage * image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [UIImage imageNamed:@"account_default"];
    });
    return image;
}

- (UIImage *)messageLinkFull {
    static UIImage *image = nil;
    if (image == nil) {
        UIImage *rawImage = [UIImage imageNamed:@"LinkFull"];
        image = [rawImage stretchableImageWithLeftCapWidth:(int)(rawImage.size.width / 2) topCapHeight:(int)(rawImage.size.height / 2)];
    }
    return image;
}

- (UIImage *)messageLinkCornerLR {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerLR"];
    }
    return image;
}

- (UIImage *)messageLinkCornerBT {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerBT"];
    }
    return image;
}

- (UIImage *)messageLinkCornerRL {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerRL"];
    }
    return image;
}

- (UIImage *)messageLinkCornerTB {
    static UIImage *image = nil;
    if (image == nil) {
        image = [UIImage imageNamed:@"LinkCornerTB"];
    }
    return image;
}

UIColor *AccentColor() {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        color = colorRGB(0x007ee5, 1.0);
    });
    return color;
}

- (UIImage *)cancelImage:(BOOL)incomming {
    static UIImage *incomingImage = nil;
    static UIImage *outgoingImg = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        incomingImage = cancelImageWithColor(colorRGB(0xffffff, 1.0),
                                             AccentColor());
        outgoingImg = cancelImageWithColor(AccentColor(),
                                             colorRGB(0xffffff, 1.0));
    });

    return incomming ? incomingImage : outgoingImg;
}

- (UIImage *)downloadImage:(BOOL)incomming {
    static UIImage *incomingImage = nil;
    static UIImage *outgoingImg = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        incomingImage = downloadImageWithColor(colorRGB(0xffffff, 1.0), AccentColor());
        outgoingImg = downloadImageWithColor(AccentColor(), colorRGB(0xffffff, 1.0));
    });

    return incomming ? incomingImage : outgoingImg;
}

- (UIImage *)checkIcon:(BOOL)isChecked {
    static UIImage *offImage = nil;
    static UIImage *onImage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        offImage = [UIImage imageNamed: @"unknown"];
        onImage = [UIImage imageNamed: @"Known"];
    });
    return isChecked ? onImage : offImage;
}

- (UIImage *)startIcon:(BOOL)isFavourite {
    static UIImage *offImage = nil;
    static UIImage *onImage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        offImage = [UIImage imageNamed: @"favourite_star_off"];
        onImage = [UIImage imageNamed: @"favourite_star_on"];
    });
    return isFavourite ? onImage : offImage;
}

- (UIImage *)playImage:(BOOL)incomming {
    static UIImage *incomingImage = nil;
    static UIImage *outgoingImg = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        incomingImage = playImageWithColor(AccentColor());
        outgoingImg = playImageWithColor([UIColor whiteColor]);
    });
    return incomming ?  incomingImage : outgoingImg;
}

- (UIImage *)pauseImage:(BOOL)incomming {
    static UIImage *incomingImage = nil;
    static UIImage *outgoingImg = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        incomingImage = pauseImageWithColor(AccentColor());
        outgoingImg = pauseImageWithColor([UIColor whiteColor]);
    });

    return incomming ? incomingImage : outgoingImg;
}

- (UIImage *)arrowRightButton {
    static UIImage *image = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        image = [UIImage imageNamed: @"arrow_right"];
    });
    return image;
}
@end
