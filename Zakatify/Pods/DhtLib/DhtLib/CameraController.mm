//
//  CameraController.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/6/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "CameraController.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CommonCrypto/CommonDigest.h>
#import "ImagePickerController.h"
#import "Utils.h"

@interface CameraController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

@implementation CameraController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    return false;
}

- (BOOL)shouldAutorotate {
    return false;
}

- (void)loadView {
    [super loadView];
    self.delegate = self;
}

- (void)dealloc {

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)__unused picker {
    id<CameraControllerDelegate> delegate = _completionDelegate;
    [delegate cameraControllerCompletedWithNoResult];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage]) {
        if (_avatarMode) {
            CGRect cropRect = [[info objectForKey:UIImagePickerControllerCropRect] CGRectValue];
            if (ABS(cropRect.size.width - cropRect.size.height) > FLT_EPSILON) {
                if (cropRect.size.width < cropRect.size.height) {
                    cropRect.origin.x -= (cropRect.size.height - cropRect.size.width) / 2;
                    cropRect.size.width = cropRect.size.height;
                } else {
                    cropRect.origin.y -= (cropRect.size.width - cropRect.size.height) / 2;
                    cropRect.size.height = cropRect.size.width;
                }
            }
            
            id<CameraControllerDelegate> delegate = _completionDelegate;
            UIImage *image = FixOrientationAndCrop([info objectForKey:UIImagePickerControllerOriginalImage], cropRect, CGSizeMake(600, 600));
            if (image != nil)
                [(id<ImagePickerControllerDelegate>)delegate imagePickerController:nil didFinishPickingWithAssets:@[image]];
            
            return;
        }
        
        id<CameraControllerDelegate> delegate = _completionDelegate;
        if ([delegate conformsToProtocol:@protocol(ImagePickerControllerDelegate)]) {
            if (_isInDocumentMode) {
                NSURL *referenceUrl = info[UIImagePickerControllerReferenceURL];
                if (referenceUrl != nil) {
                    self.view.userInteractionEnabled = false;
                    id libraryToken = [ImagePickerController preloadLibrary];
                    [ImagePickerController loadAssetWithUrl:referenceUrl completion:^(ALAsset *asset) {
                         if (asset != nil) {
                             int64_t randomId = 0;
                             arc4random_buf(&randomId, sizeof(randomId));
                             NSString *tempFileName = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSString alloc] initWithFormat:@"%lld.bin", randomId]];
                             NSOutputStream *os = [[NSOutputStream alloc] initToFileAtPath:tempFileName append:false];
                             [os open];
                             
                             ALAssetRepresentation *representation = asset.defaultRepresentation;
                             long long size = representation.size;
                             
                             uint8_t buf[128 * 1024];
                             for (long long offset = 0; offset < size; offset += 128 * 1024) {
                                 long long batchSize = MIN(128 * 1024, size - offset);
                                 NSUInteger readBytes = [representation getBytes:buf fromOffset:offset length:(NSUInteger)batchSize error:nil];
                                 [os write:buf maxLength:readBytes];
                             }
                             
                             [os close];
                             
                             NSString *mimeType = (__bridge_transfer NSString*)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)[representation UTI], kUTTagClassMIMEType);
                             [Utils dispatchOnMainAsyncSafe:^{
                                 [delegate cameraControllerCompletedWithDocument:[NSURL fileURLWithPath:tempFileName] fileName:[representation filename] mimeType:mimeType];
                             }];
                         } else {
                             [Utils dispatchOnMainAsyncSafe:^{
                                 self.view.userInteractionEnabled = true;
                             }];
                         }
                         
                         [libraryToken class];
                     }];
                }
            } else {
                UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
                if (picker.sourceType == UIImagePickerControllerSourceTypeCamera && _storeCapturedAssets) {
                    @autoreleasepool {
                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, NULL);
                    }
                }
                [(id<ImagePickerControllerDelegate>)delegate imagePickerController:nil didFinishPickingWithAssets:@[image]];
            }
        }
    } else if ([mediaType isEqualToString:(__bridge NSString *)kUTTypeMovie]) {
        id<CameraControllerDelegate> delegate = _completionDelegate;
        if ([delegate conformsToProtocol:@protocol(ImagePickerControllerDelegate)]) {
            if (_isInDocumentMode) {
                NSURL *referenceUrl = info[UIImagePickerControllerReferenceURL];
                if (referenceUrl != nil) {
                    self.view.userInteractionEnabled = false;
                    
                    id libraryToken = [ImagePickerController preloadLibrary];
                    [ImagePickerController loadAssetWithUrl:referenceUrl completion:^(ALAsset *asset) {
                         if (asset != nil) {
                             int64_t randomId = 0;
                             arc4random_buf(&randomId, sizeof(randomId));
                             NSString *tempFileName = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSString alloc] initWithFormat:@"%lld.bin", randomId]];
                             NSOutputStream *os = [[NSOutputStream alloc] initToFileAtPath:tempFileName append:false];
                             [os open];
                             
                             ALAssetRepresentation *representation = asset.defaultRepresentation;
                             long long size = representation.size;
                             
                             uint8_t buf[128 * 1024];
                             for (long long offset = 0; offset < size; offset += 128 * 1024) {
                                 long long batchSize = MIN(128 * 1024, size - offset);
                                 NSUInteger readBytes = [representation getBytes:buf fromOffset:offset length:(NSUInteger)batchSize error:nil];
                                 [os write:buf maxLength:readBytes];
                             }
                             
                             [os close];
                             
                             NSString *mimeType = (__bridge_transfer NSString*)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)[representation UTI], kUTTagClassMIMEType);
                             [Utils dispatchOnMainAsyncSafe:^{
                                 [delegate cameraControllerCompletedWithDocument:[NSURL fileURLWithPath:tempFileName] fileName:[representation filename] mimeType:mimeType];
                             }];
                         } else {
                             [Utils dispatchOnMainAsyncSafe:^{
                                 self.view.userInteractionEnabled = true;
                             }];
                         }
                         
                         [libraryToken class];
                     }];
                }
            }
        }
    }
}

- (NSString *)_dictionaryString:(NSDictionary *)dict {
    NSMutableString *string = [[NSMutableString alloc] init];
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id value, __unused BOOL *stop) {
         if ([key isKindOfClass:[NSString class]])
             [string appendString:key];
         else if ([key isKindOfClass:[NSNumber class]])
             [string appendString:[key description]];
         [string appendString:@":"];
         
         if ([value isKindOfClass:[NSString class]])
             [string appendString:value];
         else if ([value isKindOfClass:[NSNumber class]])
             [string appendString:[value description]];
         else if ([value isKindOfClass:[NSDictionary class]]) {
             [string appendString:@"{"];
             [string appendString:[self _dictionaryString:value]];
             [string appendString:@"}"];
         }
         
         [string appendString:@";"];
     }];
    
    return string;
}

+ (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)__unused contextInfo {
    [[NSFileManager defaultManager] removeItemAtPath:videoPath error:nil];
    if (error != nil) {
        [KLogger log:[NSString stringWithFormat:@"Video saving error: %@",error]];
    }
}

- (void)actionStageActionRequested:(NSString *)action options:(id)options {
    if ([action isEqualToString:@"imageCropResult"]) {
        UIImage *image = options;
        if ([options isKindOfClass:[UIImage class]]) {
            id<CameraControllerDelegate> delegate = _completionDelegate;
            if ([delegate conformsToProtocol:@protocol(ImagePickerControllerDelegate)])
                [(id<ImagePickerControllerDelegate>)delegate imagePickerController:nil didFinishPickingWithAssets:@[image]];
        }
    }
}

@end
