//
//  PhotoLibaryViewController.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "PhotoLibaryViewController.h"
#import "DhtActionControl.h"
#import "ImagePickerGalleryCell.h"
#import "BackToolbarButton.h"
#import "AssetHolder.h"
#import "Utils.h"

@interface PhotoLibaryViewController ()<UITableViewDataSource, UITableViewDelegate, DhtWatcher>

@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) NSArray *assetGroups;

@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) UIBarButtonItem *leftBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;
@property (nonatomic, weak) UIViewController *targetNavigationTitleController;
@property (nonatomic, weak) UINavigationItem *targetNavigationItem;
@property (nonatomic, strong) NSString *titleText;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIBarButtonItem *closeButtonItem;
@end

@implementation PhotoLibaryViewController

@synthesize handler = _handler;
@synthesize avatarSelectionMode = _avatarSelectionMode;

- (id)initWithAvatarSelection:(bool)avatarSelection {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _handler = [[DhtHandle alloc] initWithDelegate:self willReleaseOnMainThread:true];
        _avatarSelectionMode = avatarSelection;
    }
    return self;
}

- (void) dealloc {
    [KLogger log: [NSString stringWithFormat: @"%s",__PRETTY_FUNCTION__]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ALAssetsLibraryChangedNotification object:_assetsLibrary];
    [_handler reset];
    [actionControlInstance() removeWatcher:self];
    ALAssetsLibrary *assetsLibrary = _assetsLibrary;
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        sharedAssetsLibraryRelease();
                                        [assetsLibrary description];
                                    });
    _assetsLibrary = nil;
}

- (void) loadView {
    [super loadView];
    dispatchOnAssetsProcessingQueue(^{
        sharedAssetsLibraryRetain();
        _assetsLibrary = [ImagePickerController sharedAssetsLibrary];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetsLibraryDidChange:) name:ALAssetsLibraryChangedNotification object:_assetsLibrary];
    });
    
    _listTableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    _listTableView.backgroundColor = [UIColor whiteColor];
    _listTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _listTableView.delegate = self;
    _listTableView.dataSource = self;
    _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _listTableView.scrollIndicatorInsets = UIEdgeInsetsMake(44, 0, 44, 0);
    [self.view addSubview:_listTableView];
    self.title = NSLocalizedString(@"title.albums", nil);
    [self reloadAssets];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = [self closeButtonItem];
}

- (UIBarButtonItem *)closeButtonItem {
    if (_closeButtonItem == nil) {
        UIButton *leftbtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,50,40)];
        [leftbtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        leftbtn.imageEdgeInsets = UIEdgeInsetsMake(0, -40, 0, 0);
        [leftbtn addTarget:self
                    action:@selector(cancelButtonPressed)
          forControlEvents:UIControlEventTouchUpInside];
        _closeButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftbtn];
    }
    return _closeButtonItem;
}

#pragma --mark 
- (void) cancelButtonPressed {
    [assetHolder() clear];
    if (assetHolder().cancel) {
        assetHolder().cancel();
    }
    [self dismissViewControllerAnimated:true completion:^{
    }];
}

- (void)assetsLibraryDidChange:(NSNotification *)__unused notification {
    [self reloadAssets];
}

- (void)reloadAssets {
    dispatchOnAssetsProcessingQueue(^{
        NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
        
        [_assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, __unused BOOL *stop) {
             if (group != nil) {
                 NSURL *currentUrl = [group valueForProperty:ALAssetsGroupPropertyURL];
                 NSString *name = [group valueForProperty:ALAssetsGroupPropertyName];
                 CGImageRef posterImage = group.posterImage;
                 
                 UIImage *icon = posterImage == NULL ? nil : [[UIImage alloc] initWithCGImage:posterImage];
                 
                 [group setAssetsFilter:[ALAssetsFilter allPhotos]];
                 
                 NSMutableDictionary *groupDesc = [[NSMutableDictionary alloc] init];
                 if (name != nil) {
                     int groupType = [[group valueForProperty:ALAssetsGroupPropertyType] intValue];
                     if (groupType == ALAssetsGroupSavedPhotos) {
                         [groupDesc setObject:@"CameraRoll" forKey:@"name"];
                         [groupDesc setObject:[[NSNumber alloc] initWithInt:-1] forKey:@"order"];
                     } else {
                         [groupDesc setObject:name forKey:@"name"];
                     }
                 }
                 
                 if (icon != nil) {
                     __block UIImage *icon2 = nil;
                     __block UIImage *icon3 = nil;
                     
                     __block int enumCount = 0;
                     [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *result, __unused NSUInteger index, BOOL *stop) {
                          if (result != nil) {
                              CGImageRef thumbnail = [result thumbnail];
                              
                              if (thumbnail != NULL) {
                                  if (enumCount == 0) {
                                      icon2 = [[UIImage alloc] initWithCGImage:thumbnail];
                                      enumCount++;
                                  } else if (enumCount == 1) {
                                      icon3 = [[UIImage alloc] initWithCGImage:thumbnail];
                                      enumCount++;
                                      
                                      if (stop != NULL)
                                          *stop = true;
                                  }
                                  else if (stop != NULL)
                                      *stop = true;
                              }
                          }
                      }];
                     
                     [groupDesc setObject:icon forKey:@"icon"];
                     if (icon2 != nil)
                         groupDesc[@"icon2"] = icon2;
                     if (icon3 != nil)
                         groupDesc[@"icon3"] = icon3;
                 }
                 
                 if (currentUrl != nil)
                     [groupDesc setObject:currentUrl forKey:@"url"];
                 NSUInteger count = group.numberOfAssets;
                 [groupDesc setObject:[[NSString alloc] initWithFormat:@"%lu", (unsigned long)count] forKey:@"countString"];
                 
                 [assetGroups addObject:groupDesc];
             } else {
                 for (int i = 0; i < assetGroups.count; i++) {
                     if ([[[assetGroups objectAtIndex:i] objectForKey:@"order"] intValue] < 0) {
                         id object = [assetGroups objectAtIndex:i];
                         [assetGroups removeObjectAtIndex:i];
                         [assetGroups insertObject:object atIndex:0];
                         break;
                     }
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     _assetGroups = assetGroups;
                     [_listTableView reloadData];
                 });
             }
         } failureBlock:^(__unused NSError *error) {
             [KLogger log: @"assets access error"];
         }];
    });
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma --mark UITableView delegat
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _listTableView) {
        return _assetGroups.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)__unused indexPath {
    if (tableView == _listTableView) {
        return 85;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _listTableView) {
        if (indexPath.row < _assetGroups.count) {
            static NSString *galleryCellIdentifier = @"GC";
            ImagePickerGalleryCell *galleryCell = (ImagePickerGalleryCell *)[tableView dequeueReusableCellWithIdentifier:galleryCellIdentifier];
            if (galleryCell == nil) {
                galleryCell = [[ImagePickerGalleryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:galleryCellIdentifier];
            }
            
            NSDictionary *galleryDesc = [_assetGroups objectAtIndex:indexPath.row];
            [galleryCell setIcon:galleryDesc[@"icon"] icon2:galleryDesc[@"icon2"] icon3:galleryDesc[@"icon3"]];
            [galleryCell setTitleAccentColor:false];
            [galleryCell setTitle:[galleryDesc objectForKey:@"name"] countString:[galleryDesc objectForKey:@"countString"]];
            
            return galleryCell;
        }
    }
    
    UITableViewCell *emptyCell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCell"];
    if (emptyCell == nil) {
        emptyCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EmptyCell"];
        emptyCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return emptyCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _listTableView) {
        if (indexPath.row < _assetGroups.count) {
            NSDictionary *galleryDesc = [_assetGroups objectAtIndex:indexPath.row];
            
            ImagePickerController *imagePickerController = [[ImagePickerController alloc] initWithGroupUrl:[galleryDesc objectForKey:@"url"] groupTitle:[galleryDesc objectForKey:@"name"] avatarSelection:_avatarSelectionMode];
//            imagePickerController.delegate = _delegate;
            [self.navigationController pushViewController:imagePickerController animated:true];
        }
    }
}

@end
