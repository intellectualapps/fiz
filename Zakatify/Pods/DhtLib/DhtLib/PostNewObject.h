//
//  PostNewObject.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/6/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ImagePickerAsset;
@interface PostNewObject : NSObject
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *assetUrl;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSDate    *createDate;
- (instancetype) initWithAsset:(ImagePickerAsset *)asset;
@end
