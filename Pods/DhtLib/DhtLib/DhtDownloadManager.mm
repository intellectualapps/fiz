//
//  DhtDownloadManager.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtDownloadManager.h"
#import "DhtHandle.h"
#import "DhtActionControl.h"
DhtDownloadManager *downloadManager(){
    static DhtDownloadManager *singleton = nil;
    static dispatch_once_t onceToken;
    /*This will make this class thread safe access*/
    dispatch_once(&onceToken, ^
                  {
                      singleton = [[DhtDownloadManager alloc] init];
                  });
    
    return singleton;
}


@implementation DownloadItem
- (id)copyWithZone:(NSZone *)__unused zone
{
    DownloadItem *newItem = [[DownloadItem alloc] init];
    newItem.compareKey = _compareKey;
    newItem.path = _path;
    newItem.requestDate = _requestDate;
    newItem.progress = _progress;
    newItem.willNotifyToTargetWatcherAfterDownloadFinish = _willNotifyToTargetWatcherAfterDownloadFinish;
    newItem.groupCompareKey = _groupCompareKey;
    return newItem;
}

@end

@interface DhtDownloadManager()
@property (nonatomic, strong) NSMutableDictionary   *itemQueue;
@end

@implementation DhtDownloadManager

- (id) init {
    self = [super init];
    if (self) {
        _itemQueue = [[NSMutableDictionary alloc]init];
        _handler = [[DhtHandle alloc]initWithDelegate:self willReleaseOnMainThread:false];
    }
    return self;
}

- (void) dealloc {
    [_handler reset];
    [actionControlInstance() removeWatcher:self];
}

- (void)_notifyWatcher:(NSArray *)completedItemIds failedItemIds:(NSArray *)failedItemIds willPostToWather:(BOOL)willPost {
    NSDictionary *arguments = nil;
    if (completedItemIds != nil || failedItemIds != nil) {
        if (failedItemIds == nil)
            arguments = [[NSDictionary alloc] initWithObjectsAndKeys:completedItemIds, @"completedItemIds", nil];
        else if (completedItemIds == nil)
            arguments = [[NSDictionary alloc] initWithObjectsAndKeys:failedItemIds, @"failedItemIds", nil];
        else
            arguments = [[NSDictionary alloc] initWithObjectsAndKeys:completedItemIds, @"completedItemIds", failedItemIds, @"failedItemIds", nil];
    }
    if (willPost) {
        [actionControlInstance() dispatchResource:@"downloadManagerStateChanged" resource:_itemQueue arguments:arguments];
    }
}

- (void) requestDownloadItem:(NSString *)path
                      object:(id <BaseObject>)info
                    groupKey:(NSString *)groupKey
           willPostToWatcher:(BOOL) willPost
              changePriority:(bool)changePriority
         requestIfNotRunning:(bool)requestIfNotRunning

{
    [actionControlInstance() dispatchOnActionQueue:^{
        bool actorRunning = [actionControlInstance() requestActorStateNow:path];
        if (!actorRunning ) {
            if ([_itemQueue objectForKey:path] ==nil){
                DownloadItem *item = [[DownloadItem alloc]init];
                item.path  = [path copy];
                item.compareKey = [[info compareKey] copy];
                item.willNotifyToTargetWatcherAfterDownloadFinish = willPost || info != nil;
                if (groupKey != nil && groupKey.length >0){
                    item.groupKey = groupKey;
                }else{
                    item.groupKey = [[info groupKey] copy];
                }
                item.requestDate = CFAbsoluteTimeGetCurrent() +kCFAbsoluteTimeIntervalSince1970;
                [_itemQueue setObject:item forKey:path];
                [self _notifyWatcher:nil failedItemIds:nil willPostToWather:willPost];
                [actionControlInstance() requestActor:path options: nil flags:(changePriority)?ActionChangePriority: ActionRequestNormal watcher:self];
            } else if (actorRunning && changePriority) {
                [actionControlInstance() changeActorPriority:path];
            }

        }
    }];
}

- (void)cancelItemWithPath:(NSString *) path {
    [actionControlInstance() dispatchOnActionQueue:^{
         if (_itemQueue.count == 0)
             return;
         NSMutableArray *removeKeys = [[NSMutableArray alloc] init];
         NSMutableArray *removeMediaIds = [[NSMutableArray alloc] init];

         [_itemQueue enumerateKeysAndObjectsUsingBlock:^(NSString *path, DownloadItem *item, __unused BOOL *stop) {
              if ([item.path isEqualToString: path]) {
                  [removeKeys addObject:path];
              }
          }];

         for (NSString *path in removeKeys) {
             [_itemQueue removeObjectForKey:path];
             [actionControlInstance() removeWatcher:self fromPath:path];
         }

         [self _notifyWatcher:nil failedItemIds:removeMediaIds willPostToWather:false];
     }];
}

- (void)cancelItemsWithGroupId:(NSString *)groupId
{
    [actionControlInstance() dispatchOnActionQueue:^{
         if (_itemQueue.count == 0)
             return;
         NSMutableArray *removeKeys = [[NSMutableArray alloc] init];
         NSMutableArray *removeMediaIds = [[NSMutableArray alloc] init];

         [_itemQueue enumerateKeysAndObjectsUsingBlock:^(NSString *path, DownloadItem *item, __unused BOOL *stop) {
              if ([item.groupKey isEqualToString: groupId]) {
                  [removeMediaIds addObject:item.groupKey];
                  [removeKeys addObject:path];
              }
          }];

         for (NSString *path in removeKeys) {
             [_itemQueue removeObjectForKey:path];
             [actionControlInstance() removeWatcher:self fromPath:path];
         }

         [self _notifyWatcher:nil failedItemIds:removeMediaIds willPostToWather:false];
     }];
}

- (void)requestState:(DhtHandle *)watcherHandle {
    [actionControlInstance() dispatchOnActionQueue:^{
         [watcherHandle handleNotifyResourceDispatched:@"downloadManagerStateChanged" resource:_itemQueue arguments:@{@"requested": @true}];
     }];
}

- (void) cancelDownloadItem:(id<BaseObject>)msg {
    if (msg ==nil) {
        return;
    }
    [actionControlInstance() dispatchOnActionQueue:^{
        NSMutableArray *removeKeys = [[NSMutableArray alloc] init];
        [_itemQueue enumerateKeysAndObjectsUsingBlock:^(NSString *path, DownloadItem *item, __unused BOOL *stop)
         {
             if ([item.compareKey isEqualToString:[msg compareKey]])
             {
                 [removeKeys addObject:path];
             }
         }];

        for (NSString *path in removeKeys)
        {
            [_itemQueue removeObjectForKey:path];
            [actionControlInstance() removeWatcher:self fromPath:path];
        }

        if (removeKeys.count != 0)
            [self _notifyWatcher:nil failedItemIds:[[NSArray alloc] initWithObjects:[msg objectId], nil] willPostToWather:false];
    }];
}

- (void) actionNotifyProgress:(NSString *)path progress:(float)progress {
    if (_itemQueue !=nil) {
        DownloadItem *item = [_itemQueue objectForKey:path];
        if (item != nil) {
            item = [item copy];
            item.progress = progress;
            [_itemQueue setObject:item forKey:path];
            [self _notifyWatcher:nil failedItemIds:nil willPostToWather:item.willNotifyToTargetWatcherAfterDownloadFinish];
        }
    }
}

- (void) actionNotifyResourceDispatched:(NSString *)path resource:(id)resource arguments:(id)arguments {
}


- (void) actorProcessCompletedAtpath:(NSString *)path result:(id)result error:(NSError *)error {
    DownloadItem *item = [_itemQueue objectForKey:path];
    if (item != nil) {
        [_itemQueue removeObjectForKey:path];
        [self _notifyWatcher:(item.compareKey)?@[item.compareKey]:nil failedItemIds:nil willPostToWather:item.willNotifyToTargetWatcherAfterDownloadFinish];
    }
}
@end
