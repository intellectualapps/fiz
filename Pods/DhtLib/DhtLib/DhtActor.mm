//
//  DhtActor.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtActor.h"
#import "DhtHandle.h"
#import "KLogger.h"
#import "Utils.h"


static NSMutableDictionary *registeredRequestBuilders() {
    static NSMutableDictionary *dict = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dict = [[NSMutableDictionary alloc] init];
    });
    return dict;
}

@implementation DhtActor

- (void)dealloc {
    [KLogger log: [NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
}

+ (void)registerActorClass:(Class)requestBuilderClass {
    NSString *genericPath = [requestBuilderClass genericPath];
    if (genericPath == nil || genericPath.length == 0) {
        return;
    }
    [registeredRequestBuilders() setObject:requestBuilderClass forKey:genericPath];
}

- (void)watcherJoined:(DhtHandle *)__unused watcherHandle options:(NSDictionary *)__unused options waitingInActorQueue:(bool)__unused waitingInActorQueue {
}

+ (DhtActor *)requestBuilderForGenericPath:(NSString *)genericPath path:(NSString *)path {
    Class builderClass = [registeredRequestBuilders() objectForKey:genericPath];
    if (builderClass != nil) {
        DhtActor *builderInstance = [[builderClass alloc] initWithPath:path];
        return builderInstance;
    }
    return nil;
}

- (id) initWithPath:(NSString *)path {
    self = [super init];
    if (self) {
        _path = path;
    }
    return self;
}

/*
 Need override on subclass
 */
+ (NSString *)genericPath {
    return nil;
}

/*
 Need override on subclass
 */
- (void) cancel {
    self.cancelled =true;
}

/*
 Need override on subclass
 */
- (void) prepare:(NSDictionary *)options {
    if ([options objectForKey:@"queue_name"]) {
        self.requestQueueName = [options objectForKey:@"queue_name"];
    }
}
/*
 Need override on subclass
 */
- (void) execute:(NSDictionary *)options {
}

+ (NSOperationQueue *)operationQueue {
    static NSOperationQueue *queue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      queue = [[NSOperationQueue alloc] init];
                      [queue setMaxConcurrentOperationCount:(cpuCoreCount() > 1 ? 3 : 2)];
                  });
    return queue;
}
@end
