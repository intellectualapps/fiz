//
//  BaseImageView.m
//  kids_taxi
//
//  Created by Nguyen Van Dung on 3/14/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "BaseImageView.h"
#import "DhtActionControl.h"
#import "Utils.h"
#import "Cache.h"
@interface BaseImageView()
@property(nonatomic, strong) UIActivityIndicatorView    *activity;
@end
@implementation BaseImageView
@synthesize viewIdentifier = _viewIdentifier;
@synthesize viewStateIdentifier = _viewStateIdentifier;

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    _handler = [[DhtHandle alloc] initWithDelegate:self willReleaseOnMainThread:true];
}

- (void)dealloc{
    [self cancel];
    [_handler reset];
    [actionControlInstance() removeWatcher:self];
}

- (UIImage *)currentImage {
#if TGRemoteImageUseContents
    if (self.layer.contents != nil)
        return [UIImage imageWithCGImage:(CGImageRef)self.layer.contents];
#else
    return self.image;
#endif
    return nil;
}

- (void) loadImage:(UIImage *)image {
    self.image = image;
}

- (void)reset {
    [self cancel];
    self.image = nil;
}

/*
 - Cancel current loading image
 */
- (void) cancel {
    [self stopProgressIfNeed];
    [actionControlInstance() removeWatcher:self fromPath:self.currentUrl];
}

- (void) setViewIdentifier:(NSString *)viewIdentifier {
    _viewIdentifier = viewIdentifier;
}

- (NSString *)viewIdentifier {
    return _viewIdentifier;
}

- (void)setViewStateIdentifier:(NSString *)viewStateIdentifier {
    _viewStateIdentifier = viewStateIdentifier;
}

- (NSString *)viewStateIdentifier {
    return _viewStateIdentifier;
}

- (void)willBecomeRecycled {
    self.viewStateIdentifier  = nil;
    self.image = nil;
    [self cancel];
}

- (void)showProgressIfNeed {
    if (_needProgress) {
        if (!_activity) {
            _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            _activity.frame = CGRectMake(self.bounds.size.width / 2 - _activity.frame.size.width / 2, self.bounds.size.height/2 - _activity.frame.size.height / 2, _activity.frame.size.width, _activity.frame.size.height);
            
            [self addSubview:_activity];
        }
        [_activity startAnimating];
    }
}

- (void)stopProgressIfNeed {
    if (_activity) {
        [_activity stopAnimating];
        _activity = nil;
    }
}

- (void)loadImageAtPath:(NSString *)urlPath placeholder:(UIImage *)placeholderImage {
    NSString *path = [NSString stringWithFormat:@"/img/(%@)",urlPath];
    UIImage *thumbImage = nil;
    if (urlPath.length > 0 && !_donotCacheImage) {
        thumbImage = [[Cache shareInstance] cachedThumbnail:path];
    }
    if (thumbImage) {
        [Utils dispatchOnMainAsyncSafe:^{
            self.image = thumbImage;
        }];
    } else {
        if (placeholderImage != nil) {
            [Utils dispatchOnMainAsyncSafe:^{
                self.image = placeholderImage;
            }];
        }
        if (urlPath.length > 0) {
            _currentUrl = path;
            [self showProgressIfNeed];
            [actionControlInstance() dispatchOnActionQueue:^{
                [actionControlInstance() requestActor:path options:nil flags:ActionRequestNormal watcher:self];
            }];
        }
    }
}

- (void)actorProcessCompletedAtpath:(NSString *)path result:(id)result error:(NSError *)error {
    if ([path isEqualToString: _currentUrl] == true) {
        [self stopProgressIfNeed];
        if ([result isKindOfClass:[UIImage class]]) {
            [Utils dispatchOnMainAsyncSafe:^{
                if (self.donotCacheImage) {
                    self.image = result;
                } else {
                    self.image = [[Cache shareInstance] cacheThumbnail:result url:path cacheSizeType:CGSizeMake(default_avatar_witdh, default_avatar_witdh)];
                }
            }];
        }
    }
}
@end
