//
//  BackToolbarButton.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackToolbarButton : UIButton
@property (nonatomic, strong) UILabel *buttonTitleLabel;
@property (nonatomic, strong) NSString *buttonTitle;
@property (nonatomic, assign) float arrowOffset;
- (instancetype)initWithLightModeAndimage:(UIImage *)image;
- (void)setButtonTitle:(NSString *)buttonTitle;
@end
