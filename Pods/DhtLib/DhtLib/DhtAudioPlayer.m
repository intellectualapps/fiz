//
//  DhtAudioPlayer.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/4/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtAudioPlayer.h"
#import "DhtQueue.h"
#import "KLogger.h"
@interface DhtAudioPlayer()<AVAudioPlayerDelegate> {
    AVAudioPlayer *_audioPlayer;
    bool _audioSessionIsActive;
    NSTimer         *_timer;
    BOOL            _isPaused;
}

@end

@implementation DhtAudioPlayer

- (instancetype) initWithPath:(NSString *) path {
    self = [super init];
    if (self) {
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:nil];
        if (_audioPlayer == nil) {
            [KLogger log: @"init audio fail"];
            [self cleanupWithError];
        }
    }
    return self;
}

- (instancetype)initWithData:(NSData *)data {
    self = [super init];
    if (self) {
        _audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:nil];
        if (_audioPlayer == nil) {
            [KLogger log: @"init audio fail"];
            [self cleanupWithError];
        }
        [_audioPlayer prepareToPlay];
        [_audioPlayer setVolume:1.0];
        _audioPlayer.delegate = self;
    }
    return self;
}

- (BOOL)isReady {
    return (_audioPlayer != nil);
}

- (void) dealloc {
    [self cleanup];
}

- (void) removeInlineDelegateObject:(id<DhtInlineAudioPlayerDelegate>)object {
    id <DhtInlineAudioPlayerDelegate> currentDelegate  = _inLineDelegate;
    if (currentDelegate == object) {
        _inLineDelegate = nil;
    }
}

- (BOOL) isPaused {
    return _isPaused;
}

+ (DhtQueue *)_playerQueue {
    static DhtQueue *queue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        queue = [[DhtQueue alloc] initWithName:"Dht.audioPlayerQueue"];
    });
    
    return queue;
}

- (void)cleanupWithError {
    [self cleanup];
}

- (void)cleanup {
    AVAudioPlayer *audioPlayer = _audioPlayer;
    _audioPlayer.delegate = nil;
    _audioPlayer = nil;
    [[DhtAudioPlayer _playerQueue] dispatchOnQueue:^{
         [audioPlayer stop];
     }];
    
    [self _endAudioSessionFinal];
}

+ (BOOL)isHeadsetPluggedIn {
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}

- (void)_beginAudioSession {
    [[DhtAudioPlayer _playerQueue] dispatchOnQueue:^
     {
         if (!_audioSessionIsActive) {
             __autoreleasing NSError *error = nil;
             AVAudioSession *audioSession = [AVAudioSession sharedInstance];
             bool overridePort =  ![DhtAudioPlayer isHeadsetPluggedIn];
             if (![audioSession setCategory:overridePort ? AVAudioSessionCategoryPlayAndRecord :AVAudioSessionCategoryPlayback error:&error]) {
                 
             } else if (![audioSession setActive:true error:&error]) {
                 
             } else {
                 if (![audioSession overrideOutputAudioPort:overridePort ? AVAudioSessionPortOverrideSpeaker : AVAudioSessionPortOverrideNone error:&error])
                     _audioSessionIsActive = true;
             }
         }
     }];
}

- (void)_endAudioSessionFinal {
    bool audioSessionIsActive = _audioSessionIsActive;
    _audioSessionIsActive = false;
    
    [[DhtAudioPlayer _playerQueue] dispatchOnQueue:^{
         if (audioSessionIsActive) {
             __autoreleasing NSError *error = nil;
             AVAudioSession *audioSession = [AVAudioSession sharedInstance];
             if (![audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error]) {
             }
             if (![audioSession setActive:false error:&error]) {
             }
         }
     }];
}

- (void) wakeupTimer {
    [self invalidTimer];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self
                                            selector:@selector(updateCurrentTime)
                                            userInfo:nil repeats:true];
}

- (void)play {
    _isPaused =false;
    [self playFromPosition:-1];
    [self updateCurrentTime];
    [self wakeupTimer];
}

- (void) updateCurrentTime {
    [self postUpdatePlaybackPosition:false];
    
}

- (NSTimeInterval)currentTime {
    return [_audioPlayer currentTime];
}

- (NSTimeInterval)currentPositionSync:(bool)sync {
    __block NSTimeInterval result = 0.0;
    
    dispatch_block_t block = ^ {
        result = [_audioPlayer currentTime];
    };
    
    if (sync)
        [[DhtAudioPlayer _playerQueue] dispatchOnQueue:block synchronous:true];
    else
        block();
    
    return result;
}

- (float)playbackPositionSync:(bool)sync {
    NSTimeInterval duration = [_audioPlayer duration];
    if (duration > 0.1)
        return (float)([self currentPositionSync:sync] / duration);
    
    return 0.0f;
}

- (float)playbackPosition:(NSTimeInterval *)timestamp sync:(bool)sync {
    if (timestamp != NULL) {
        *timestamp = [[NSDate date]timeIntervalSince1970];
    }
    return [self playbackPositionSync:sync];
}

- (void)postUpdatePlaybackPosition:(bool)sync {
    NSTimeInterval timestamp = [[NSDate date]timeIntervalSince1970];
    float position = [self playbackPosition:&timestamp sync:sync];
    id <DhtInlineAudioPlayerDelegate> delegate = _inLineDelegate;
    if ([delegate respondsToSelector:@selector(mediaPlaybackStateUpdated:playbackPosition:timestamp:preciseDuration:)]) {
        [delegate mediaPlaybackStateUpdated:_isPaused playbackPosition:position timestamp:timestamp preciseDuration:[_audioPlayer duration]];
    }
}

- (void)pause {
    _isPaused = true;
    [[DhtAudioPlayer _playerQueue] dispatchOnQueue:^{
         [_audioPlayer pause];
     }];
    [self invalidTimer];
}

- (void) invalidTimer {
    if (_timer != nil) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)stop {
    _isPaused =true;
    [[DhtAudioPlayer _playerQueue] dispatchOnQueue:^{
         [_audioPlayer stop];
     }];
    [self invalidTimer];
    [self cleanup];
}

- (NSTimeInterval)duration {
    return [_audioPlayer duration];
}

- (void)playFromPosition:(NSTimeInterval)position {
    [[DhtAudioPlayer _playerQueue] dispatchOnQueue:^{
         [self _beginAudioSession];
         if (position >= 0.0)
             [_audioPlayer setCurrentTime:position];
         [_audioPlayer play];
     }];
}

- (void)_notifyFinished {
    id<DhtAudioPlayerDelegate> delegate = _delegate;
    if ([delegate respondsToSelector:@selector(audioPlayerDidFinishPlaying:)])
        [delegate audioPlayerDidFinishPlaying:self];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)__unused player successfully:(BOOL)__unused flag {
    [self _notifyFinished];
}
@end
